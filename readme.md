# Mars Invaders

![](Images/Titre.png)

## Description

**Space invaders** est un jeu d'arcade sorti en 1978. Considéré comme le premier archétype du shoot them up, il est aussi l'un des titres les plus influents et célèbres de l'histoire du jeu vidéo.

[En savoir plus](https://fr.wikipedia.org/wiki/Space_Invaders)

## But du jeu

Le joueur, représenté par un canon en bas de l'écran, doit éliminer des vagues d'aliens en leur tirant dessus avant que ces-derniers n'atteignent les habitations, et ce en évitant leur tirs. Le joueur perd également la partie lorsque toutes les habitations sont détruites.

## Guide d'utilisation  

Pour jouer à *Mars Invaders*, il faudra vous munir du simulateur [Mars](http://courses.missouristate.edu/KenVollmar/mars/).
1. Chargez le fichier invader.s dans Mars
2. Pour avoir l'affichage, ouvrez le menu « Tools » et sélectionnez « Bitmap Display ». Dans la fenêtre qui apparaît, fixez les deux paramètres « Unit Width & Height in Pixels » à la valeur 4 et cliquez sur « Connect to MIPS ».
3. Pour l’entrée clavier du jeu, ouvrez le menu « Tools » et sélectionnez « Keyboard and Display MMIO Simulator » et cliquez sur « Connect to MIPS »
4. Vous pouvez maintenant compiler le fichier puis l'executer.

**ATTENTION :** Pour que la sauvegarde des highscores fonctionne, il faut que vous placiez les fichiers "Scores_facile.minv", "Scores_normal.minv" et "Scores_difficile.minv" dans le **même répertoire que le simulateur Mars** !

## Commandes

### Dans les menu

* *'4'* : Délacer le curseur vers le haut ou la gauche selon le menu
* *'5'* : Valider dans les menus ou changer la lettre dans l'entrée du Pseudo
* *'6'* : Délacer le curseur vers le bas ou la droite selon le menu

### En Jeu:

* *'4'* : Déplacer le canon vers la gauche.
* *'5'* : Faire feu (*Boom*).
* *'6'* : Déplacer le canon vers la droite.

## Choix de la difficulté

Ce jeu possède 3 modes de difficulté :

### Mode Facile

Nombre de vies du joueur : 3
Vitesse des ennemis ralentie
Le temps entre les tirs est diminué.

### Mode Normal [Choix par défaut]

Nombre de vies du joueur : 2

### Mode Difficile

Nombre de vies du joueur : 1
Les aliens se déplacent bien plus vite...
Le rayon d'explosion des tirs des aliens est augmenté !!
Le temps entre les tirs est augmenté.

## Score

Le score final est déterminé en fonction de différents paramètres :
* Le joueur commence la partie avec des points en fonction de la difficulté
choisie (500/750/1000)
* Chaque vie restante rapporte des points (150/250/600).
* Chaque morceau de bâtiment encore debout après l'invasion rapporte également
des points (1/2/7)
* En cas de défaite, chaque aliens en vie fait perdre des points (20/10/5)
* De plus, le joueur perd des points avec le temps, dépéchez-vous donc
de repousser cette invasion si vous voulez atteindre notre score !!

Chaque niveau de difficulté possède son propre tableau des scores. Quelque soit
votre niveau, vous n'avez donc aucune excuses pour ne pas y figurer...

## Screenshots

|      Menu       |        Jeu      |
| :-------------: | :-------------: |
| ![](Images/Difficulté.png) | ![](Images/Jeu.png) |
| Victoire | Scores |
| ![](Images/Victoire.png) | ![](Images/Highscores.png) |

## Auteurs

[BESNAULT-CLERICE Audric](https://gitlab.unistra.fr/abesnaultclerice) :
Programmeur, Graphiste 👽  
[ZERBIB Timothée](https://gitlab.unistra.fr/tzerbib) : Programmeur, ♪ Sound designer ♫
