################################################################################
#                                _____                     _                   #
#        /\/\   __ _ _ __ ___    \_   \_ ____   ____ _  __| | ___ _ __         #
#       /    \ / _` | '__/ __|    / /\/ '_ \ \ / / _` |/ _` |/ _ \ '__|        #
#      / /\/\ \ (_| | |  \__ \ /\/ /_ | | | \ V / (_| | (_| |  __/ |           #
#      \/    \/\__,_|_|  |___/ \____/ |_| |_|\_/ \__,_|\__,_|\___|_|           #
#                                                                              #
#                                                                              #
#                              ▄▄████▄▄                                        #
#                            ▄██████████▄                                      #
#                          ▄██▄██▄██▄██▄██▄                                    #
#                            ▀█▀  ▀▀  ▀█▀                                      #
#                                                                              #
################################################################################

#
# Vous n'avez pas besoin de modifier ou de comprendre le code qui vous est fourni.
#
# Les paramètres et valeur de retour des fonctions sont indiqués pour chaque
# fonction.
#
# Pour appeler les fonctions, passez les paramètres dans les registres de $a0 à
# $a3, dans cet ordre, pour les arguments des fonctions.
# Si la fonction renvoie une valeur, elle se trouvera dans $v0.
#
#
# Partie .text (Le programme)
# ** Ne pas modifier **

.text

j main

# Fonction cleanPartOfScreen
# Arguments : $a0 Abscisse du bord haut_gauche à effacer
#           : $a1 Ordonnée du bord haut_gauche à effacer
#           : $a2 Taille à effacer en abscisse (positif)
#           : $a3 Taille à effacer en ordonnée (positif)
# Retour : Aucun
# Condition : Rester dans les bornes [0,128[ en abscisse et  [0,64[ en ordonnée.
# Effet de bord : Dessine du noir sur la partie ciblée

 .globl cleanPartOfScreen       # -- Begin function cleanPartOfScreen
cleanPartOfScreen:                      # @cleanPartOfScreen
#Prologue
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp

#Corps
 beqz $a3, $BB0_5     #Jump à la fin si rien à effacer
 sll $at, $a0, 2      #2 shift left soit *4
 sll $v0, $a1, 9      #Permet de savoir le decalage du framebuffer
 addu $t0, $v0, $at
 #lui $v0, 4097        #adresse du premier pixel frameBuffer
 la $v0 frameBuffer	   #Lors de cette exec $at est remplacé !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 add $v0, $v0, $t0
 li $v1 0

$BB0_2:
 move $a0, $v0          #reinitialise a0 et a1 pour la boucle et saute si pas de remplissage à faire
 move $a1, $a2
 beqz $a2, $BB0_4

$BB0_3:
 sw $zero, 0($a0)       #remplit une ligne de noir
 addiu $a1, $a1, -1     #a1 le nombre de pixel restant et a0 la Position
 addiu $a0, $a0, 4
 bnez $a1, $BB0_3

$BB0_4:
 addiu $v1, $v1, 1      #Ajoute 1 au compteur de ligne et replace le framebuffer
 addiu $v0, $v0, 512
 bne $v1, $a3, $BB0_2

$BB0_5:

#Epilogue
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

# Fonction clean_screen
# Arguments : Aucun
# Retour : Aucun
# Effet de bord : Dessine du noir sur tout l'écran

 .globl clean_screen            # -- Begin function clean_screen
clean_screen:                           # @clean_screen
#Prologue
  addiu $sp, $sp, -24
  sw $ra, 20($sp)
  sw $fp, 16($sp)

#Corps
  li $a0 0
  li $a1 0
  li $a2 128
  li $a3 64
  jal cleanPartOfScreen

#Epilogue
  lw $fp, 16($sp)
  lw $ra, 20($sp)
  addiu $sp, $sp, 24
  jr $ra


# Fonction enemyBoxPosY
# Arguments : $a0 Le numéro de la ligne de l'extraterrestre dont on veut connaître la position
# Retour : $v0 La ligne du coin supérieur droit de l'extraterrestre
# En relation : enemyRows (Partie .data) pour la longueur d'un extraterrestre

 .globl enemyBoxPosY            # -- Begin function enemyBoxPosY
enemyBoxPosY:                           # @enemyBoxPosY

#Prologue
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp

#Corps
 sll $at, $a0, 1
 sll $v0, $a0, 3
 addu $at, $v0, $at
 lui $v0, %hi(boxTopPosY)
 lw $v0, %lo(boxTopPosY)($v0)
 addu $v0, $v0, $at

#Epilogue
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra


# Fonction enemyBoxPosX
# Arguments : $a0 Le numéro de la colonne de l'extraterrestre dont on veut connaître la position
# Retour : $v0 La colonne du coin supérieur droit de l'extraterrestre
# En relation : enemyCols (Partie .data) pour la largeur d'un extraterrestre

 .globl enemyBoxPosX            # -- Begin function enemyBoxPosX
enemyBoxPosX:                           # @enemyBoxPosX

#Prologue
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp

#Corps
 sll $at, $a0, 4
 subu $at, $at, $a0
 lui $v0, %hi(boxTopPosX)
 lw $v0, %lo(boxTopPosX)($v0)
 addu $v0, $v0, $at

#Epilogue
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

# Fonction random_int_range
# Arguments : $a0 Borne inférieure
#           : $a1 Borne supérieure
# Retour : $v0 Un entier dans l'intervalle [$a0,$a1[
# Condition : $a0 < $a1

 .globl random_int_range        # -- Begin function random_int_range
random_int_range:                       # @random_int_range

#Prologue
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp

#Corps
 move $v1, $a0
 subu $a2, $a1, $a0
 addiu $v0, $zero, 42
 addiu $a0, $zero, 0
 addu $a1, $zero, $a2
 syscall
 move $v0, $a0
 addu $v0, $v0, $v1

#Epilogue
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

# Fonction print_int
# Arguments : $a0 Un entier à afficher
# Retour : Aucun
# Effet de bord : Affiche un entier dans la console de MARS

 .globl print_int               # -- Begin function print_int
print_int:                              # @print_int

#Prologue
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp

#Corps
 move $v1, $a0
 addiu $v0, $zero, 1
 addu $a0, $zero, $v1
 syscall

#Epilogue
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

# Fonction print_string
# Arguments : $a0 L'adresse d'une chaîne de caractère.
# Retour : Aucun
# Effet de bord : Affiche la chaîne de caractère dans la console de MARS

 .globl print_string            # -- Begin function print_string
print_string:                           # @print_string

#Prologue
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp

#Corps
 move $v1, $a0
 addiu $v0, $zero, 4
 addu $a0, $zero, $v1
 syscall

#Epilogue
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

# Fonction read_int
# Arguments : Aucun
# Retour : $v0 L'entier entré au clavier par l'utilisateur

 .globl read_int                # -- Begin function read_int
read_int:                               # @read_int

#Prologue
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp

#Corps
 addiu $v0, $zero, 5
 syscall
 move $v0, $v0

#Epilogue
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

# Fonction keyStroke
# Arguments : Aucun
# Retour : $v0 La valeur 1 si la touche '4' a été la dernière pressée dans le simulateur de clavier MARS.
#              La valeur 2 si la touche '6' a été la dernière pressée dans le simulateur de clavier MARS.
#              La valeur 3 si la touche '5' a été la dernière pressée dans le simulateur de clavier MARS.
#              La valeur 0 sinon.
# Attention : La touche n'est consommée qu'une fois, après quoi la valeur retournée est 0.
#             Si plusieurs touches ont été pressées entre deux appels à keyStroke, seul la dernière sera prise en compte.

 .globl keyStroke               # -- Begin function keyStroke
keyStroke:                              # @keyStroke

#Prologue
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp

#Corps
 lui $at, %hi(inputKeyboard)
 lw $at, %lo(inputKeyboard)($at)
 lw $v0, 0($at)
 sw $zero, 0($at)
 addiu $v0, $v0, -52
 sltiu $at, $v0, 3
 beqz $at, $BB6_2
 sll $at, $v0, 2
 lui $v0, %hi($switch.table.keyStroke)
 addiu $v0, $v0, %lo($switch.table.keyStroke)
 addu $at, $v0, $at
 lw $v0, 0($at)

#Epilogue
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

$BB6_2:
 addiu $v0, $zero, 0
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

# Fonction alienTurn
# Arguments : Aucun
# Retour : Aucun
# Effet de bord : Déplace les extraterrestres et crée un projectile en direction du joueur aléatoirement.

 .globl alienTurn               # -- Begin function alienTurn
alienTurn:                              # @alienTurn
 addiu $sp, $sp, -56
 sw $ra, 52($sp)
 sw $fp, 48($sp)
 sw $s7, 44($sp)
 sw $s6, 40($sp)
 sw $s5, 36($sp)
 sw $s4, 32($sp)
 sw $s3, 28($sp)
 sw $s2, 24($sp)
 sw $s1, 20($sp)
 sw $s0, 16($sp)
 move $fp, $sp
 
 #lui $at, %hi(boxDirX)
 #lw $v0, %lo(boxDirX)($at)
 lw $v0 boxDirX
 
 
 #addiu $at, $zero, 2
 li $at 2
 # Si boxDirX = 2 déplacement alors on est en déplacement vers la droite ou 
 beq $v0, $at, $alien_turn_deplacement_bas_droite
#$BB7_1:
 # Si boxDirX = 1 déplacement à gauche
 # Sinon skip les mouvement de monstre // impossble normalement
 #addiu $at, $zero, 1
 #li $at 1
 #bne $v0, $at, $BB7_22
#$BB7_2:

 # Si boite tout à gauche, aller au changement de sens à gauche
 lui $at, %hi(boxTopPosX)
 lw $v0, %lo(boxTopPosX)($at)
 beqz $v0, $alien_turn_deplacement_bas_gauche
 
 # s'assure que la boite d'alien ne dépasse pas
 lui $s0, %hi(boxMovementX)
 lw $s1, %lo(boxMovementX)($s0)
 subu $at, $v0, $s1
 sltiu $at, $at, 129
 move $v1, $s1
 movz $v1, $v0, $at
 sw $v1, %lo(boxMovementX)($s0)
 
 #test inutile
 lui $s2, %hi(enemiesPerRow)
 #lw $at, %lo(enemiesPerRow)($s2)
 #beqz $at, $BB7_6
 
 # Des inits pour plus tard
 addiu $s3, $zero, 0
 addiu $s4, $zero, 12
 lui $s5, %hi(boxTopPosX)
 lui $s6, %hi(boxSizeY)
 lui $s7, %hi(boxTopPosY)
 
$alien_turn_boucle_supression_deplacement_gauche:
 lw $a2, %lo(boxMovementX)($s0)
 lw $at, %lo(boxTopPosX)($s5)
 subu $at, $at, $a2
 addu $a0, $s4, $at
 lw $a3, %lo(boxSizeY)($s6)
 lw $a1, %lo(boxTopPosY)($s7)
 jal cleanPartOfScreen
 addiu $s3, $s3, 1
 lw $at, %lo(enemiesPerRow)($s2)
 sltu $at, $s3, $at
 addiu $s4, $s4, 15
 bnez $at, $alien_turn_boucle_supression_deplacement_gauche
#$BB7_6:
 # remet l'ancienne valeur dans boxMovementX
 # dans le cas ou elle à changé lié à la partie s'assurer de pas dépasser
 lw $at, %lo(boxMovementX)($s0)
 sw $s1, %lo(boxMovementX)($s0)
 # met à jour la position de la boite d'alien en X
 lui $v0, %hi(boxTopPosX)
 lw $v1, %lo(boxTopPosX)($v0)
 subu $at, $v1, $at
 sw $at, %lo(boxTopPosX)($v0)
 j $BB7_22
 
 
$alien_turn_deplacement_bas_droite:
 # Si Alien pas tout à droite, aller au déplacement à droite
 lui $at, %hi(boxTopPosX)
 lw $v0, %lo(boxTopPosX)($at)
 lui $at, %hi(boxSizeX)
 lw $v1, %lo(boxSizeX)($at)
 addu $a1, $v1, $v0
 addiu $at, $zero, 127
 bne $a1, $at, $alien_turn_deplacement_droite
 
 # test inutile
 lui $s0, %hi(enemiesRows)
 #lw $at, %lo(enemiesRows)($s0)
 #beqz $at, $BB7_11
 
 # Des inits pour plus tard
 addiu $s1, $zero, 0
 lui $s2, %hi(boxTopPosY)
 lui $s3, %hi(boxMovementY)
 lui $s4, %hi(boxSizeX)
 lui $s5, %hi(boxTopPosX)
 addiu $s6, $zero, 0
 
$alien_turn_boucle_supression_deplacement_bas_droite:
 lw $at, %lo(boxTopPosY)($s2)
 addu $a1, $s1, $at
 lw $a3, %lo(boxMovementY)($s3)
 lw $a2, %lo(boxSizeX)($s4)
 lw $a0, %lo(boxTopPosX)($s5)
 jal cleanPartOfScreen
 addiu $s6, $s6, 1
 lw $at, %lo(enemiesRows)($s0)
 sltu $at, $s6, $at
 addiu $s1, $s1, 10
 bnez $at, $alien_turn_boucle_supression_deplacement_bas_droite
#$BB7_11:
 # change la direction du déplacement
 lui $at, %hi(boxDirX)
 addiu $v0, $zero, 1
 sw $v0, %lo(boxDirX)($at)
 
 # change la valeur de la position de la boite d'alien en Y
 lui $at, %hi(boxMovementY)
 lw $at, %lo(boxMovementY)($at)
 lui $v0, %hi(boxTopPosY)
 lw $v1, %lo(boxTopPosY)($v0)
 addu $at, $v1, $at
 sw $at, %lo(boxTopPosY)($v0)
 j $BB7_22
 
 
$alien_turn_deplacement_droite:
 # s'assure que le boite ne dépasse pas à droite
 lui $a0, %hi(boxMovementX)
 lw $s0, %lo(boxMovementX)($a0)
 addu $at, $s0, $a1
 sltiu $at, $at, 128
 move $a1, $s0
 bnez $at, $BB7_14
 addiu $at, $zero, 128
 subu $at, $at, $v0
 not $v0, $v1
 addu $a1, $at, $v0
$BB7_14:
 sw $a1, %lo(boxMovementX)($a0)
 
 # test inutile
 lui $s1, %hi(enemiesPerRow)
 #lw $at, %lo(enemiesPerRow)($s1)
 #beqz $at, $BB7_17
 
 #Des inits pour la suite
 addiu $s2, $zero, 0
 lui $s3, %hi(boxTopPosX)
 lui $s4, %hi(boxSizeY)
 lui $s5, %hi(boxMovementX)
 lui $s6, %hi(boxTopPosY)
 addiu $s7, $zero, 0
 
$alien_turn_boucle_supression_deplacement_droite:
 lw $at, %lo(boxTopPosX)($s3)
 addu $a0, $s2, $at
 lw $a3, %lo(boxSizeY)($s4)
 lw $a2, %lo(boxMovementX)($s5)
 lw $a1, %lo(boxTopPosY)($s6)
 jal cleanPartOfScreen
 addiu $s7, $s7, 1
 lw $at, %lo(enemiesPerRow)($s1)
 sltu $at, $s7, $at
 addiu $s2, $s2, 15
 bnez $at, $alien_turn_boucle_supression_deplacement_droite
#$BB7_17:

 # remet l'ancienne valeur dans boxMovementX lié
 # dans le cas ou elle à changé lié à la partie s'assurer de pas dépasser
 lui $at, %hi(boxMovementX)
 lw $v0, %lo(boxMovementX)($at)
 sw $s0, %lo(boxMovementX)($at)
 
 # met à jour la position de la boite des alien
 lui $at, %hi(boxTopPosX)
 lw $v1, %lo(boxTopPosX)($at)
 addu $v0, $v1, $v0
 sw $v0, %lo(boxTopPosX)($at)
 j $BB7_22
 
 
$alien_turn_deplacement_bas_gauche:
 # test inutile
 lui $s0, %hi(enemiesRows)
 #lw $at, %lo(enemiesRows)($s0)
 #beqz $at, $BB7_21
 addiu $s1, $zero, 0
 lui $s2, %hi(boxTopPosY)
 lui $s3, %hi(boxMovementY)
 lui $s4, %hi(boxSizeX)
 lui $s5, %hi(boxTopPosX)
 addiu $s6, $zero, 0
$alien_turn_boucle_supression_deplacement_bas_gauche:
 lw $at, %lo(boxTopPosY)($s2)
 addu $a1, $s1, $at
 lw $a3, %lo(boxMovementY)($s3)
 lw $a2, %lo(boxSizeX)($s4)
 lw $a0, %lo(boxTopPosX)($s5)
 jal cleanPartOfScreen
 addiu $s6, $s6, 1
 lw $at, %lo(enemiesRows)($s0)
 sltu $at, $s6, $at
 addiu $s1, $s1, 10
 bnez $at, $alien_turn_boucle_supression_deplacement_bas_gauche
#$BB7_21:
 # change la direction du déplacement
 lui $at, %hi(boxDirX)
 addiu $v0, $zero, 2
 sw $v0, %lo(boxDirX)($at)
 
 # change la valeur de la position de la boite d'alien en Y
 lui $at, %hi(boxMovementY)
 lw $at, %lo(boxMovementY)($at)
 lui $v0, %hi(boxTopPosY)
 lw $v1, %lo(boxTopPosY)($v0)
 addu $at, $v1, $at
 sw $at, %lo(boxTopPosY)($v0)


$BB7_22:
 #test inutile
 lui $v0, %hi(enemiesRows)
 #lw $at, %lo(enemiesRows)($v0)
 #beqz $at, $fin_alien_turn
 
 addiu $v1, $zero, 0
 lui $a0, 4097
 lui $a1, %hi(enemiesPerRow)
 lui $at, %hi(enemyRowType)
 addiu $a2, $at, %lo(enemyRowType)
 lui $at, %hi(enemiesLife)
 addiu $a3, $at, %lo(enemiesLife)
 lui $t0, %hi(boxTopPosY)
 lui $t1, %hi(boxTopPosX)
 lui $t2, %hi(enemies_shape)
 lui $t3, %hi(enemyColor)
 addiu $t4, $zero, 48
 addiu $t5, $zero, 8
 addiu $s0, $zero, 0
$alien_turn_affiche_ligne_alien:
 move $s7 $a0 # new
 move $a0 $v1 # new
 jal change_enemies_color # new
 move $a0 $s7 # news
 #lw $at, %lo(enemiesPerRow)($a1)
 #beqz $at, $BB7_33
 sll $t6, $v1, 2
 addu $t7, $a2, $t6
 addiu $t8, $zero, 0
 move $t9, $a0
$alien_turn_affiche_alien:
 sll $at, $v1, 4
 addu $at, $at, $t6
 addu $at, $a3, $at
 sll $gp, $t8, 2
 addu $at, $at, $gp
 lw $at, 0($at)
 beqz $at, $alien_turn_afficher_alien_suivant
 lw $at, %lo(boxTopPosY)($t0)
 sll $at, $at, 9
 addu $at, $t9, $at
 lw $gp, %lo(boxTopPosX)($t1)
 sll $gp, $gp, 2
 addu $gp, $at, $gp
 addiu $s0, $t2, %lo(enemies_shape)
 addiu $s1, $zero, 0
$alien_turn_affiche_ligne_pixel_un_alien:
 addiu $s2, $zero, 0
$alien_turn_affiche_un_pixel_alien:
 lw $at, 0($t7)
 sll $s3, $at, 7
 sll $at, $at, 8
 addu $at, $at, $s3
 addu $at, $s2, $at
 addu $at, $s0, $at
 lw $at, 0($at)
 addiu $s3, $s2, 4
 lui $t3, %hi(enemyColor)
 lw $s4, %lo(enemyColor)($t3)
 movz $s4, $zero, $at
 addu $at, $gp, $s2
 sw $s4, 0($at)
 move $s2, $s3
 bne $s3, $t4, $alien_turn_affiche_un_pixel_alien
 addiu $s0, $s0, 48
 addiu $s1, $s1, 1
 addiu $gp, $gp, 512
 bne $s1, $t5, $alien_turn_affiche_ligne_pixel_un_alien
 addiu $s0, $zero, 1
$alien_turn_afficher_alien_suivant:
 lw $at, %lo(enemiesPerRow)($a1)
 addiu $t8, $t8, 1
 sltu $at, $t8, $at
 addiu $t9, $t9, 60
 bnez $at, $alien_turn_affiche_alien
 
 
#$BB7_33: 
 # si il reste une ligne, recommencer
 lw $at, %lo(enemiesRows)($v0)
 addiu $v1, $v1, 1
 sltu $at, $v1, $at
 addiu $a0, $a0, 5120
 bnez $at, $alien_turn_affiche_ligne_alien
 
 
 beqz $s0, $fin_alien_turn
 lui $s1, %hi(enemiesPerRow)
 lui $s2, %hi(enemiesRows)
 lui $at, %hi(enemiesLife)
 addiu $s3, $at, %lo(enemiesLife)
$BB7_36:
 lw $a1, %lo(enemiesPerRow)($s1)
 addiu $a0, $zero, 0
 jal random_int_range
 lw $a0, %lo(enemiesRows)($s2)
 sll $at, $a0, 2
 addu $at, $at, $a0
 addu $at, $v0, $at
 sll $at, $at, 2
 addu $at, $s3, $at
 addiu $a1, $at, -20
 sll $at, $a0, 1
 sll $v1, $a0, 3
 addu $at, $v1, $at
 addiu $v1, $at, 8
 move $a2, $a0
$BB7_37:
 addiu $a2, $a2, -1
 sltu $at, $a2, $a0
 beqz $at, $BB7_40
 addiu $at, $a1, -20
 addiu $v1, $v1, -10
 lw $a3, 0($a1)
 move $a1, $at
 beqz $a3, $BB7_37
 j $BB7_42
$BB7_40:
 bnez $s0, $BB7_36
 j $fin_alien_turn
$BB7_42:
 lui $at, %hi(shotsInFlight)
 lw $a0, %lo(shotsInFlight)($at)
 sll $a1, $a0, 2
 sll $a0, $a0, 3
 addu $a0, $a0, $a1
 lui $a1, %hi(shootingPosition)
 addiu $a1, $a1, %lo(shootingPosition)
 addu $a0, $a1, $a0
 sll $a1, $v0, 4
 subu $v0, $a1, $v0
 lui $a1, %hi(boxTopPosX)
 lw $a1, %lo(boxTopPosX)($a1)
 addu $v0, $v0, $a1
 addiu $v0, $v0, 6
 sw $v0, 0($a0)
 lui $a1, %hi(boxTopPosY)
 lw $a1, %lo(boxTopPosY)($a1)
 lui $a2, %hi(projectileColor)
 lui $a3, 4097
 addu $v1, $a1, $v1
 sw $v1, 4($a0)
 sw $zero, 8($a0)
 sll $v0, $v0, 2
 sll $v1, $v1, 9
 addu $v0, $v1, $v0
 addu $v0, $v0, $a3
 lw $v1, %lo(projectileColor)($a2)
 sw $v1, 0($v0)
 lw $v0, %lo(shotsInFlight)($at)
 addiu $v0, $v0, 1
 sw $v0, %lo(shotsInFlight)($at)
$fin_alien_turn:
 move $sp, $fp
 lw $s0, 16($sp)
 lw $s1, 20($sp)
 lw $s2, 24($sp)
 lw $s3, 28($sp)
 lw $s4, 32($sp)
 lw $s5, 36($sp)
 lw $s6, 40($sp)
 lw $s7, 44($sp)
 lw $fp, 48($sp)
 lw $ra, 52($sp)
 addiu $sp, $sp, 56
 jr $ra

# Fonction printBuilding
# Arguments : Aucun
# Retour : Aucun
# Effet de bord : Affiche les bâtiments à l'écran

 .globl printBuilding           # -- Begin function printBuilding
printBuilding:                          # @printBuilding
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp
 lui $at, %hi(building)
 addiu $v0, $at, %lo(building)
 addiu $v1, $zero, 0
 lui $at, %hi(buildingPosX)
 addiu $a0, $at, %lo(buildingPosX)
 lui $at, 4097
 ori $a1, $at, 25088
 lui $a2, %hi(buildingColor)
 addiu $a3, $zero, 32
 addiu $t0, $zero, 6
 addiu $t1, $zero, 4
$BB8_1:
 sll $at, $v1, 2
 addu $at, $a0, $at
 lw $at, 0($at)
 sll $at, $at, 2
 addu $t2, $at, $a1
 move $t3, $v0
 addiu $t4, $zero, 0
$BB8_2:
 addiu $t5, $zero, 0
$BB8_3:
 addu $at, $t3, $t5
 lw $at, 0($at)
 lw $t6, %lo(buildingColor)($a2)
 movz $t6, $zero, $at
 addu $at, $t2, $t5
 addiu $t5, $t5, 4
 sw $t6, 0($at)
 bne $t5, $a3, $BB8_3
 addiu $t2, $t2, 512
 addiu $t4, $t4, 1
 addiu $t3, $t3, 32
 bne $t4, $t0, $BB8_2
 addiu $v1, $v1, 1
 addiu $v0, $v0, 192
 bne $v1, $t1, $BB8_1
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

# Fonction printShooter
# Arguments : $a0 Valeur 1 si le canon doit se déplacer à gauche
#                 Valeur 2 si le canon doit se déplacer à droite
#                 Valeur 3 si le canon doit tirer un laser de la mort qui tue.
# Retour : Aucun
# Effet de bord : Met à jour la position du joueur, l'affiche à l'écran et crée un projectile si nécessaire.

 .globl printShooter            # -- Begin function printShooter
printShooter:                           # @printShooter
 addiu $sp, $sp, -32
 sw $ra, 28($sp)
 sw $fp, 24($sp)
 sw $s1, 20($sp)
 sw $s0, 16($sp)
 move $fp, $sp
 addiu $at, $zero, 2
 move $s0, $a0
 beq $a0, $at, $BB9_4
$BB9_1:
 addiu $at, $zero, 1
 bne $s0, $at, $BB9_8
$BB9_2:
 lui $s1, %hi(shooterPosX)
 lw $v0, %lo(shooterPosX)($s1)
 beqz $v0, $BB9_8
 addiu $a0, $v0, 12
 addiu $a1, $zero, 58
 addiu $a2, $zero, 1
 addiu $a3, $zero, 5
 jal cleanPartOfScreen
 lw $at, %lo(shooterPosX)($s1)
 addiu $at, $at, -1
 sw $at, %lo(shooterPosX)($s1)
 j $BB9_8
$BB9_4:
 lui $s1, %hi(shooterPosX)
 lw $a0, %lo(shooterPosX)($s1)
 addiu $at, $a0, 14
 sltiu $at, $at, 129
 beqz $at, $BB9_6
 addiu $a1, $zero, 58
 addiu $a2, $zero, 1
 addiu $a3, $zero, 5
 jal cleanPartOfScreen
 lw $at, %lo(shooterPosX)($s1)
 addiu $at, $at, 1
 sw $at, %lo(shooterPosX)($s1)
 j $BB9_8
$BB9_6:
 addiu $s1, $zero, 115
 beq $a0, $s1, $BB9_8
 subu $a2, $s1, $a0
 addiu $a1, $zero, 58
 addiu $a3, $zero, 5
 jal cleanPartOfScreen
 lui $at, %hi(shooterPosX)
 sw $s1, %lo(shooterPosX)($at)
$BB9_8:
 lui $at, %hi(shooter)
 addiu $v0, $at, %lo(shooter)
 addiu $v1, $zero, 0
 lui $a0, %hi(shooterPosX)
 lui $a1, %hi(shooterColor)
 lui $a2, 4097
 addiu $a3, $zero, 13
 addiu $t0, $zero, 5
$BB9_9:
 addiu $t1, $v1, 58
 move $t2, $v0
 addiu $t3, $zero, 0
$BB9_10:
 lw $at, %lo(shooterPosX)($a0)
 addu $at, $t3, $at
 lw $t4, %lo(shooterColor)($a1)
 lw $t5, 0($t2)
 movz $t4, $zero, $t5
 sll $at, $at, 2
 sll $t5, $t1, 9
 addu $at, $t5, $at
 addu $at, $at, $a2
 sw $t4, 0($at)
 addiu $t3, $t3, 1
 addiu $t2, $t2, 4
 bne $t3, $a3, $BB9_10
 addiu $v1, $v1, 1
 addiu $v0, $v0, 52
 bne $v1, $t0, $BB9_9
 addiu $at, $zero, 3
 bne $s0, $at, $BB9_14
 lui $at, %hi(shotsInFlight)
 lw $v0, %lo(shotsInFlight)($at)
 sll $v1, $v0, 2
 sll $v0, $v0, 3
 addu $v0, $v0, $v1
 lui $v1, %hi(shootingPosition)
 addiu $v1, $v1, %lo(shootingPosition)
 addu $v0, $v1, $v0
 lui $v1, %hi(shooterPosX)
 lw $v1, %lo(shooterPosX)($v1)
 addiu $v1, $v1, 6
 addiu $a0, $zero, 57
 sw $a0, 4($v0)
 sw $v1, 0($v0)
 addiu $a0, $zero, 1
 sw $a0, 8($v0)
 lui $v0, 4097
 ori $v0, $v0, 29184
 sll $v1, $v1, 2
 addu $v0, $v1, $v0
 lui $v1, %hi(projectileColor)
 lw $v1, %lo(projectileColor)($v1)
 sw $v1, 0($v0)
 lw $v0, %lo(shotsInFlight)($at)
 addiu $v0, $v0, 1
 sw $v0, %lo(shotsInFlight)($at)
$BB9_14:
 move $sp, $fp
 lw $s0, 16($sp)
 lw $s1, 20($sp)
 lw $fp, 24($sp)
 lw $ra, 28($sp)
 addiu $sp, $sp, 32
 jr $ra

# Fonction sleep
# Argument : $a0 Un entier
# Retour : Aucun
# Crée un délai en répétant une opération qui ne fait rien.

 .globl sleep                   # -- Begin function sleep
sleep:                                  # @sleep
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp
 sll $at, $a0, 3
 sll $v0, $a0, 4
 addu $at, $v0, $at
 sll $v0, $a0, 10
 subu $v0, $v0, $at
 sw $zero, 0($fp)
 lw $at, 0($fp)
 sltu $at, $at, $v0
 beqz $at, $BB10_2
$BB10_1:
 lw $at, 0($fp)
 addiu $at, $at, 1
 sw $at, 0($fp)
 lw $at, 0($fp)
 sltu $at, $at, $v0
 bnez $at, $BB10_1
$BB10_2:
 move $sp, $fp
 lw $fp, 4($sp)
 addiu $sp, $sp, 8
 jr $ra

# Fonction resolveAndPrintShots
# Argument : Aucun
# Retour : Aucun
# Effet de bord : Met à jour le tableau enemiesLife si un extraterrestre est touché.
#                 Met la valeur 1 dans l'entier has_lost si le joueur se fait toucher.
#                 Met à jour le tableau "building" avec la valeur zéro si un projectile percute un bâtiment.
#                 Met à jour la position des projectiles.

 .globl resolveAndPrintShots    # -- Begin function resolveAndPrintShots
resolveAndPrintShots:                   # @resolveAndPrintShots
 addiu $sp, $sp, -104
 sw $ra, 100($sp)
 sw $fp, 96($sp)
 sw $s7, 92($sp)
 sw $s6, 88($sp)
 sw $s5, 84($sp)
 sw $s4, 80($sp)
 sw $s3, 76($sp)
 sw $s2, 72($sp)
 sw $s1, 68($sp)
 sw $s0, 64($sp)
 move $fp, $sp
 lui $t7, %hi(shotsInFlight)
 lw $at, %lo(shotsInFlight)($t7)
 beqz $at, $BB11_76
 lui $at, %hi(shootingPosition)
 addiu $s5, $at, %lo(shootingPosition)
 addiu $s2, $zero, 0
 lui $at, %hi(enemies_shape)
 addiu $at, $at, %lo(enemies_shape)
 sw $at, 28($fp)
 lui $t8, %hi(building)
 addiu $at, $t8, %lo(building)
 sw $at, 24($fp)
 lui $at, %hi(buildingPosX)
 addiu $s4, $at, %lo(buildingPosX)
 addiu $s3, $zero, 12
 lui $at, %hi(shooter)
 addiu $at, $at, %lo(shooter)
 sw $at, 40($fp)
 lui $at, %hi(enemyRowType)
 addiu $at, $at, %lo(enemyRowType)
 sw $at, 60($fp)
 sw $s5, 32($fp)
 j $BB11_17
$BB11_2:
 lw $v1, 44($fp)
 sltiu $at, $v1, 64
 bnez $at, $BB11_9
 lw $at, %lo(shotsInFlight)($t7)
 beqz $at, $BB11_75
 lw $at, 0($s7)
 sll $at, $at, 2
 lw $v0, 56($fp)
 lw $v0, 0($v0)
 sll $v0, $v0, 9
 addu $at, $v0, $at
 lui $v0, 4097
 addu $at, $at, $v0
 sw $zero, 0($at)
 lw $at, %lo(shotsInFlight)($t7)
 addiu $v0, $at, -1
 sltu $at, $s2, $v0
 move $v1, $s5
 move $a0, $s2
 beqz $at, $BB11_8
$BB11_5:
 addiu $a0, $a0, 1
 addiu $a1, $zero, 0
$BB11_6:
 addu $at, $v1, $a1
 lw $a2, 12($at)
 addiu $a1, $a1, 4
 sw $a2, 0($at)
 bne $a1, $s3, $BB11_6
 addiu $v1, $v1, 12
 bne $a0, $v0, $BB11_5
$BB11_8:
 sw $v0, %lo(shotsInFlight)($t7)
 j $BB11_75
$BB11_9:
 sll $at, $v0, 9
 lw $a0, 36($fp)
 addu $at, $at, $a0
 lui $v0, 4097
 addu $at, $at, $v0
 sw $zero, 0($at)
 sll $at, $v1, 9
 addu $at, $at, $a0
 addu $at, $at, $v0
 lui $v0, %hi(projectileColor)
 lw $v0, %lo(projectileColor)($v0)
 sw $v0, 0($at)
 lw $at, 56($fp)
 sw $v1, 0($at)
 j $BB11_75
$BB11_10:
 lw $at, %lo(shotsInFlight)($t7)
 beqz $at, $BB11_16
 lw $at, 0($s7)
 sll $at, $at, 2
 lw $v0, 56($fp)
 lw $v0, 0($v0)
 sll $v0, $v0, 9
 addu $at, $v0, $at
 lui $v0, 4097
 addu $at, $at, $v0
 sw $zero, 0($at)
 lw $at, %lo(shotsInFlight)($t7)
 addiu $v0, $at, -1
 sltu $at, $s2, $v0
 move $v1, $s5
 move $a0, $s2
 beqz $at, $BB11_15
$BB11_12:
 addiu $a0, $a0, 1
 addiu $a1, $zero, 0
$BB11_13:
 addu $at, $v1, $a1
 lw $a2, 12($at)
 addiu $a1, $a1, 4
 sw $a2, 0($at)
 bne $a1, $s3, $BB11_13
 addiu $v1, $v1, 12
 bne $a0, $v0, $BB11_12
$BB11_15:
 sw $v0, %lo(shotsInFlight)($t7)
$BB11_16:
 addiu $at, $zero, 1
 lui $v0, %hi(has_lost)
 sw $at, %lo(has_lost)($v0)
 j $BB11_75
$BB11_17:
 sll $at, $s2, 2
 sll $v0, $s2, 3
 addu $at, $v0, $at
 lw $v0, 32($fp)
 addu $s7, $v0, $at
 lw $a2, 0($s7)
 lw $v0, 4($s7)
 sll $at, $v0, 3
 addu $at, $a2, $at
 lw $v1, 8($s7)
 sll $a0, $v0, 4
 sll $a1, $v0, 5
 addu $a0, $a1, $a0
 sll $at, $at, 2
 xori $v1, $v1, 1
 addiu $t0, $zero, 1
 addiu $a1, $zero, -1
 movz $t0, $a1, $v1
 lw $v1, 24($fp)
 addu $at, $v1, $at
 addu $a1, $t0, $v0
 lw $v1, 28($fp)
 addu $v1, $v1, $a0
 sll $a3, $a2, 2
 sll $a0, $t0, 4
 sll $t1, $t0, 5
 addu $t9, $t1, $a0
 sw $a3, 36($fp)
 addu $t3, $v1, $a3
 addiu $v1, $s7, 4
 sw $v1, 56($fp)
 sw $a1, 44($fp)
 addu $gp, $a1, $t0
 addiu $t5, $at, -1568
 move $t6, $v0
 sw $t1, 48($fp)
 sw $gp, 52($fp)
$BB11_18:
 beq $t6, $gp, $BB11_2
 sltiu $at, $t6, 64
 beqz $at, $BB11_2
 addiu $a0, $t6, -49
 sltiu $at, $a0, 6
 addiu $a1, $zero, 0
 beqz $at, $BB11_49
 addiu $a3, $zero, 0
 move $v1, $t5
$BB11_22:
 sll $at, $a3, 2
 addu $at, $s4, $at
 lw $t2, 0($at)
 subu $a1, $a2, $t2
 sltiu $at, $a1, 8
 beqz $at, $BB11_24
 sll $at, $a3, 6
 sll $t4, $a3, 7
 addu $at, $t4, $at
 addiu $t4, $t8, %lo(building)
 addu $at, $t4, $at
 sll $t4, $a0, 5
 addu $at, $at, $t4
 sll $t4, $a1, 2
 addu $at, $at, $t4
 lw $at, 0($at)
 bnez $at, $BB11_26
$BB11_24:
 addiu $a3, $a3, 1
 sltiu $at, $a3, 4
 addiu $v1, $v1, 192
 bnez $at, $BB11_22
 addiu $a1, $zero, 0
 j $BB11_49
$BB11_26:
 sw $t9, 20($fp)
 lui $at, %hi(shotRadiusBuilding)
 lw $t7, %lo(shotRadiusBuilding)($at)
 sll $at, $t7, 3
 sll $a3, $t7, 5
 addu $at, $a3, $at
 sll $gp, $t2, 2
 sll $s0, $t7, 1
 addu $t8, $s0, $t7
 subu $at, $v1, $at
 subu $v1, $v1, $a3
 subu $a3, $a2, $s0
 subu $t9, $a3, $t2
 subu $t4, $v1, $gp
 subu $ra, $at, $gp
 ori $s0, $s0, 1
 addiu $s1, $zero, 0
$BB11_27:
 sltu $at, $t7, $s1
 beqz $at, $BB11_35
 subu $v1, $s1, $t7
 subu $a3, $t8, $s1
 sltu $at, $a3, $v1
 bnez $at, $BB11_41
 addu $at, $s1, $a0
 subu $t2, $at, $t7
 move $s6, $t9
 move $gp, $ra
$BB11_30:
 sltiu $at, $t2, 6
 beqz $at, $BB11_33
 sltiu $at, $s6, 8
 beqz $at, $BB11_33
 sw $zero, 0($gp)
$BB11_33:
 addiu $v1, $v1, 1
 sltu $at, $a3, $v1
 addiu $s6, $s6, 1
 addiu $gp, $gp, 4
 beqz $at, $BB11_30
 j $BB11_41
$BB11_35:
 subu $v1, $t7, $s1
 addu $a3, $s1, $t7
 sltu $at, $a3, $v1
 bnez $at, $BB11_41
 addu $at, $s1, $a0
 subu $t2, $at, $t7
 move $s6, $a1
 move $gp, $t4
$BB11_37:
 sltiu $at, $t2, 6
 beqz $at, $BB11_40
 sltiu $at, $s6, 8
 beqz $at, $BB11_40
 sw $zero, 0($gp)
$BB11_40:
 addiu $v1, $v1, 1
 sltu $at, $a3, $v1
 addiu $s6, $s6, 1
 addiu $gp, $gp, 4
 beqz $at, $BB11_37
$BB11_41:
 addiu $t9, $t9, 1
 addiu $ra, $ra, 36
 addiu $a1, $a1, -1
 addiu $s1, $s1, 1
 addiu $t4, $t4, 28
 bne $s1, $s0, $BB11_27
 lui $t7, %hi(shotsInFlight)
 lw $at, %lo(shotsInFlight)($t7)
 addiu $a1, $zero, 1
 beqz $at, $BB11_48
 lw $at, 0($s7)
 sll $at, $at, 2
 lw $v1, 56($fp)
 lw $v1, 0($v1)
 sll $v1, $v1, 9
 addu $at, $v1, $at
 lui $v1, 4097
 addu $at, $at, $v1
 sw $zero, 0($at)
 lw $at, %lo(shotsInFlight)($t7)
 addiu $v1, $at, -1
 sltu $at, $s2, $v1
 move $a0, $s5
 move $a3, $s2
 lw $t9, 20($fp)
 lw $gp, 52($fp)
 lui $t8, %hi(building)
 beqz $at, $BB11_47
$BB11_44:
 addiu $a3, $a3, 1
 addiu $t2, $zero, 0
$BB11_45:
 addu $at, $a0, $t2
 lw $t4, 12($at)
 addiu $t2, $t2, 4
 sw $t4, 0($at)
 bne $t2, $s3, $BB11_45
 addiu $a0, $a0, 12
 bne $a3, $v1, $BB11_44
$BB11_47:
 sw $v1, %lo(shotsInFlight)($t7)
 j $BB11_49
$BB11_48:
 lui $t8, %hi(building)
 lw $t9, 20($fp)
 lw $gp, 52($fp)
$BB11_49:
 addiu $v1, $t6, -58
 sltiu $at, $v1, 5
 beqz $at, $BB11_53
 bnez $a1, $BB11_53
 lui $at, %hi(shooterPosX)
 lw $at, %lo(shooterPosX)($at)
 subu $a0, $a2, $at
 sltiu $at, $a0, 13
 beqz $at, $BB11_53
 sll $at, $v1, 2
 sll $a3, $v1, 3
 addu $at, $a3, $at
 sll $v1, $v1, 6
 subu $at, $v1, $at
 lw $v1, 40($fp)
 addu $at, $v1, $at
 sll $v1, $a0, 2
 addu $at, $at, $v1
 lw $at, 0($at)
 bnez $at, $BB11_10
$BB11_53:
 bnez $a1, $BB11_67
 lui $at, %hi(enemiesRows)
 lw $at, %lo(enemiesRows)($at)
 beqz $at, $BB11_67
 move $t1, $t9
 lui $at, %hi(boxTopPosY)
 lw $t7, %lo(boxTopPosY)($at)
 sll $at, $t7, 4
 sll $v1, $t7, 5
 addu $at, $v1, $at
 subu $at, $t3, $at
 lui $v1, %hi(boxTopPosX)
 lw $t8, %lo(boxTopPosX)($v1)
 sll $v1, $t8, 2
 subu $t2, $at, $v1
 lui $at, %hi(enemiesLife)
 addiu $gp, $at, %lo(enemiesLife)
 lui $at, %hi(enemiesRows)
 lw $s0, %lo(enemiesRows)($at)
 lui $at, %hi(enemiesPerRow)
 lw $s1, %lo(enemiesPerRow)($at)
 addiu $ra, $zero, 0
$BB11_56:
 sll $at, $ra, 1
 sll $v1, $ra, 3
 addu $at, $v1, $at
 addu $a1, $t7, $at
 addiu $at, $a1, 8
 sltu $at, $t6, $at
 beqz $at, $BB11_65
 sltu $at, $t6, $a1
 bnez $at, $BB11_65
 beqz $s1, $BB11_65
 sll $at, $ra, 2
 lw $v1, 60($fp)
 addu $a3, $v1, $at
 addiu $v1, $zero, 0
 move $t9, $t2
 move $a0, $t8
 move $s6, $gp
$BB11_60:
 sltu $at, $a2, $a0
 bnez $at, $BB11_64
 lw $at, 0($s6)
 beqz $at, $BB11_64
 addiu $at, $a0, 12
 sltu $at, $a2, $at
 beqz $at, $BB11_64
 lw $at, 0($a3)
 sll $t4, $at, 7
 sll $at, $at, 8
 addu $at, $at, $t4
 addu $at, $t9, $at
 lw $at, 0($at)
 bnez $at, $BB11_69
$BB11_64:
 addiu $v1, $v1, 1
 sltu $at, $v1, $s1
 addiu $t9, $t9, -60
 addiu $a0, $a0, 15
 addiu $s6, $s6, 4
 bnez $at, $BB11_60
$BB11_65:
 addiu $ra, $ra, 1
 sltu $at, $ra, $s0
 addiu $t2, $t2, -480
 addiu $gp, $gp, 20
 bnez $at, $BB11_56
 addiu $a1, $zero, 0
 lui $t7, %hi(shotsInFlight)
 lui $t8, %hi(building)
 move $t9, $t1
 lw $t1, 48($fp)
 lw $gp, 52($fp)
$BB11_67:
 addu $t3, $t3, $t9
 addu $t5, $t5, $t1
 addu $t6, $t6, $t0
 beqz $a1, $BB11_18
 j $BB11_75
$BB11_69:
 lw $t7, 0($s6) #new
 subiu $t7 $t7 1 #new
 bnez $t7 resolveAndPrintShots_skip_clean_alien #new
 addiu $a2, $zero, 12
 addiu $a3, $zero, 8
 jal cleanPartOfScreen
resolveAndPrintShots_skip_clean_alien: #new
 sw $t7, 0($s6)
 lui $t7, %hi(shotsInFlight)
 lw $at, %lo(shotsInFlight)($t7)
 lui $t8, %hi(building)
 beqz $at, $BB11_75
 lw $at, 0($s7)
 sll $at, $at, 2
 lw $v0, 56($fp)
 lw $v0, 0($v0)
 sll $v0, $v0, 9
 addu $at, $v0, $at
 lui $v0, 4097
 addu $at, $at, $v0
 sw $zero, 0($at)
 lw $at, %lo(shotsInFlight)($t7)
 addiu $v0, $at, -1
 sltu $at, $s2, $v0
 move $v1, $s5
 move $a0, $s2
 beqz $at, $BB11_74
$BB11_71:
 addiu $a0, $a0, 1
 addiu $a1, $zero, 0
$BB11_72:
 addu $at, $v1, $a1
 lw $a2, 12($at)
 addiu $a1, $a1, 4
 sw $a2, 0($at)
 bne $a1, $s3, $BB11_72
 addiu $v1, $v1, 12
 bne $a0, $v0, $BB11_71
$BB11_74:
 sw $v0, %lo(shotsInFlight)($t7)
$BB11_75:
 lw $at, %lo(shotsInFlight)($t7)
 addiu $s2, $s2, 1
 sltu $at, $s2, $at
 addiu $s5, $s5, 12
 bnez $at, $BB11_17
$BB11_76:
 move $sp, $fp
 lw $s0, 64($sp)
 lw $s1, 68($sp)
 lw $s2, 72($sp)
 lw $s3, 76($sp)
 lw $s4, 80($sp)
 lw $s5, 84($sp)
 lw $s6, 88($sp)
 lw $s7, 92($sp)
 lw $fp, 96($sp)
 lw $ra, 100($sp)
 addiu $sp, $sp, 104
 jr $ra

# Fonction quit
# Argument : Aucun
# Retour : Ne retourne jamais **insérer rire diabolique** !
# Effet de bord : Fin du programme

quit:                                   # @quit
 addiu $sp, $sp, -8
 sw $fp, 4($sp)
 move $fp, $sp
 addiu $v0, $zero, 10
 syscall

################################################################################
#                                                                              #
# Partie .data (Les données globales)                                          #
#                                                                              #
# Vous pouvez jouer avec certains paramètres pour voir ce qu'il se passe :D    #
# Avant de toucher à ces valeurs, faites une version fonctionnelle de votre    #
# projet.                                                                      #
#                                                                              #
################################################################################

# Bitmap de l'écran. Chaque pixel correspond à une valeur sur 32bits 0xaabbccdd
# où bb est la couleur rouge, cc la verte et dd la bleue sur 256 valeurs.
.data
 .globl frameBuffer
 .align 2
frameBuffer:
 .space 32768

# Nom du fichier de score chargé
filename:
 .asciiz "Scores_normal.minv"

# Nom du fichier contenant les scores en mode 'Facile'
score_facile:
 .asciiz "Scores_facile.minv"

# Nom du fichier contenant les scores en mode 'Normal'
score_normal:
 .asciiz "Scores_normal.minv"

# Nom du fichier contenant les scores en mode 'Difficile'
score_difficile:
 .asciiz "Scores_difficile.minv"

# Nombre de scores sauvegardes par fichier
nombreDeScoresSauvegardes:
 .word 5

# Nom du joueur
text_pseudo:
 .word 65065065

# Juste un saut de ligne
saut_ligne:
 .asciiz "\n"

# dev/null
devnull:
 .space 10

# L'adresse où lire les données clavier virtuel
 .data
 .globl inputKeyboard
 .align 2
inputKeyboard:
 .word 4294901764

# Compteur de vie du joueur
 .globl players_life
players_life:
 .word 2

# Vies  du joueur en facile
players_life_easy:
 .word 3

# Vies  du joueur en difficile
players_life_hard:
 .word 1

# Vies  du joueur en Normal
players_life_normal:
 .word 2


# Tabelau des valeurs des lignes de l'Alien de l'écran de départ
  .globl Tableau_Invaders_Start
  .align 2
Tableau_Invaders_Start:
  .word 0x03c0
  .word 0x0ff0
  .word 0x1ff8
  .word 0x3ffc
  .word 0x6db6
  .word 0xffff
  .word 0x399c
  .word 0x1008


#Tableau des valeurs des caractères pour l'affichage
 .globl Alphabet
 .align 2
Alphabet:
.word  32078418   	#A
.word  29979228    	#B
.word  13189708    	#C
.word  29968988    	#D
.word  32010782    	#E
.word  32010768    	#F
.word  13130318    	#G
.word  19495506    	#H
.word  14815374    	#I
.word  14748236    	#J
.word  19554962	  	#K
.word  17318430    	#L
.word  18732593    	#M
.word  19749458    	#N
.word  15288908    	#O
.word  32076304    	#P
.word  13191759    	#Q
.word  29979282    	#R
.word  15216732    	#S
.word  32641156    	#T
.word  19483212    	#U
.word  19482948    	#V
.word  22730410    	#W
.word  19739218    	#X
.word  19478604    	#Y
.word  31535646    	#Z
.word  13191756    	#0
.word  2304066    	#1
.word  29438494    	#2
.word  29438044    	#3
.word  4475842    	#4
.word  32012380    	#5
.word  15235660    	#6
.word  31527176    	#7
.word  13185612    	#8
.word  15284316    	#9
.word  4329476		  #!
.word  31537416		  #?
.word  131200		    #:
.word  11512810		  ##
.word  13241833		  # Alien 1 part 1
.word  12927876		  # Alien 1 part 2
.word  33204224		  # Alien 1 part 3
.word  31805440		  # Alien 1 part 4
.word  3226991		  # Alien 2 part 1
.word  17330844		  # Alien 2 part 2
.word  7842624		  # Alien 2 part 3
.word  26104512		  # Alien 2 part 4
.word  105638		    # Humain part 1
.word  549144		    # Humain part 2
.word  3386432		  # Humain part 3
.word  17584640		  # Humain part 4

# Perdu ou pas perdu, telle est la question!
 .globl has_lost
 .align 2
 has_lost:
 .word 0                       # 0x0

# Message défaite
  .globl message_defaite
 message_defaite:
 .asciiz  "\n Game Over !! \n Score : "

# Message victoire
  .globl message_victoire
 message_victoire:
 .asciiz  "\n Victoire !! \n Score : "

# Message touché
  .globl message_touche
 message_touche:
  .asciiz "\n Touché"

# Temps entre les tirs du joueur
  .globl Cooldown
 Cooldown:
  .word 10

  .globl CooldownFacile
 CooldownFacile:
  .word 10

  .globl CooldownNormal
 CooldownNormal:
  .word 16

  .globl CooldownDifficile
 CooldownDifficile:
  .word 22

# Temps d'invulnérabilité après avoir été touché
  .globl temps_invulnérabilité
temps_invulnérabilité:
  .word 20

# SCORE
  .globl PointsBase # Points au début de la partie
 PointsBase:
  .word 750

  .globl PointsBaseFacile # Points au début de la partie
 PointsBaseFacile:
  .word 500

  .globl PointsBaseNormal # Points au début de la partie
 PointsBaseNormal:
  .word 750

  .globl PointsBaseDifficile # Points au début de la partie
 PointsBaseDifficile:
  .word 1000

  .globl PointsPixelsBatiments # Points gagnés par pixel de batiment restant
 PointsPixelsBatiments:
  .word 2

 .globl PointsPixelsBatimentsFacile # Points gagnés par pixel de batiment restant
PointsPixelsBatimentsFacile:
 .word 1

 .globl PointsPixelsBatimentsNormal # Points gagnés par pixel de batiment restant
PointsPixelsBatimentsNormal:
 .word 2

.globl PointsPixelsBatimentsDifficile # Points gagnés par pixel de batiment restant
PointsPixelsBatimentsDifficile:
  .word 7

  .globl CoutPointsAliensVivants # Points perdus par aliens vivants
 CoutPointsAliensVivants:
  .word 10

 .globl CoutPointsAliensVivantsFacile # Points perdus par aliens vivants
CoutPointsAliensVivantsFacile:
  .word 20

  .globl CoutPointsAliensVivantsNormal # Points perdus par aliens vivants
CoutPointsAliensVivantsNormal:
  .word 10

.globl CoutPointsAliensVivantsDifficile # Points perdus par aliens vivants
CoutPointsAliensVivantsDifficile:
  .word 5

  .globl PointsVieRestantes # Points gagnés par vie restantes
 PointsVieRestantes:
  .word 250

  .globl PointsVieRestantesFacile # Points gagnés par vie restantes
 PointsVieRestantesFacile:
  .word 150

  .globl PointsVieRestantesNormal # Points gagnés par vie restantes
 PointsVieRestantesNormal:
  .word 250

  .globl PointsVieRestantesDifficile # Points gagnés par vie restantes
 PointsVieRestantesDifficile:
  .word 600


# Le nombre de lignes maximum prise par un extraterrestre
 .globl enemyRows
 .align 2
enemyRows:
 .word 8                       # 0x8

# Le nombre de colonnes maximum prise par un extraterrestre
 .globl enemyCols
 .align 2
enemyCols:
 .word 12                      # 0xc

# Le rayon d'explosion, quand un projectile explose sur un bâtiment
 .data
 .globl shotRadiusBuilding
 .align 2
shotRadiusBuilding:
 .word 2                       # 0x2

# Rayon d'explosion en mode difficile
 .globl shotRadiusBuildingHard
 .align 2
shotRadiusBuildingHard:
 .word 5

# Rayon d'explosion en mode difficile
 .globl shotRadiusBuildingNormal
 .align 2
shotRadiusBuildingNormal:
 .word 2


# Le nombre de déplacement horizontal parcouru par les ennemies
 .globl boxMovementX
 .align 2
boxMovementX:
 .word 3                       # 0x3

# Le nombre de déplacement vertical parcouru par les ennemies
 .globl boxMovementY
 .align 2
boxMovementY:
 .word 2                       # 0x2

#Variable d'ajustement de la vitesse des ennemis
Variable_Vitesse:
 .word 6

#Variable d'ajustement de la vitesse des ennemis
Variable_Vitesse_facile:
 .word 8

#Variable d'ajustement de la vitesse des ennemis
Variable_Vitesse_normal:
 .word 6

#Variable d'ajustement de la vitesse des ennemis
Variable_Vitesse_difficile:
  .word 4

# La position horizontale de la boîte invisible des ennemies
 .globl boxTopPosX
 .align 2
boxTopPosX:
 .word 0                       # 0x0

# La position verticale de la boîte invisible des ennemies
 .globl boxTopPosY
 .align 2
boxTopPosY:
 .word 0                      # 0x0

# Le nombre d'ennemies par ligne
 .data
 .globl enemiesPerRow
 .align 2
enemiesPerRow:
 .word 5                       # 0x5

# Le nombre de lignes d'ennemies
 .globl enemiesRows
 .align 2
enemiesRows:
 .word 3                       # 0x3

# Le type d'extraterrestre sur une ligne donnée (Entier 0, 1 ou 2)
 .globl enemyRowType
 .align 2
enemyRowType:
 .word 1                       # 0x1
 .word 2                       # 0x2
 .word 0                       # 0x0

# La vie des extraterrestres (Tableau [enemiesRows][enemiesPerRow])
 .globl enemiesLife
 .align 2
enemiesLife:
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1
 .word 2                       # 0x1

# La couleur des extraterrestres
 .globl enemyColor
 .align 2
enemyColor:
 .word 16777215                # 0xffffff

 .globl enemyColor1
 .align 2
enemyColor1:
 .word 14164018                # 0xd82032

 .globl enemyColor2
 .align 2
enemyColor2:
 .word 3720920                # 0x38c6d8


 .globl enemyColor3
 .align 2
enemyColor3:
 .word 14358711                # 0xdb18b7


# La couleur du canon du joueur
 .globl shooterColor
 .align 2
shooterColor:
 .word 14493479                # 0xdd2727

# La couleur des projectiles
 .globl projectileColor
 .align 2
projectileColor:
 .word 16772190                # 0xffec5e

# La couleur des bâtiments
 .globl buildingColor
 .align 2
buildingColor:
 .word 7786848                # 0x76d160

# Couleur du message de victoire
Couleur_Message_Victoire:
  .word 11993088		#0xb70000

Couleur_Message_Score:
  .word 16750848		#0xf2ff00

Couleur_Score:
  .word 15924992

Couleur_Curseur:
  .word 0xffe100		#jaune

Couleur_Normal:
  .word 0xff9000		#Orange

Couleur_Blanc:
  .word 0xffffff		#Blanc

# La vie des bâtiments
 .globl building
 .align 2
building:
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1 mid bat 1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1 Fin bat 1
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1 Mid bat 2
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 mid bat 2
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1 fin bat 2
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1 mid bat 3
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1 fin bat 3
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 mid bat 4
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1

# La vie des bâtiments
 .globl building_save
 .align 2
building_save:
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1 mid bat 1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1 Fin bat 1
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1 Mid bat 2
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 mid bat 2
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1 fin bat 2
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1 mid bat 3
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1 fin bat 3
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 mid bat 4
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1

# Le nombre de projectiles dans le jeu en ce moment
# ** Ne pas modifier **
 .globl shotsInFlight
 .align 2
shotsInFlight:
 .word 0                       # 0x0

# La structure de donnée des projectiles
# ** Ne pas modifier **
 .globl shootingPosition
 .align 2
shootingPosition:
 .space 1200

# La taille horizontale de la boîte invisible des extraterrestres
 .globl boxSizeX
 .align 2
boxSizeX:
 .word 0                       # 0x0

# La taille verticale de la boîte invisible des extraterrestres
 .globl boxSizeY
 .align 2
boxSizeY:
 .word 0                       # 0x0

# La direction de la boîte des extraterrestres (2 = droite, 1 = gauche)
 .data
 .globl boxDirX
 .align 2
boxDirX:
 .word 2                       # 0x2

# Les formes des ennemies pour l'affichage (Tableau [3][8][12])
# ** Modifier si vous avez du style, sinon laissez cela aux professionnels **
  .data
 .align 2
enemies_shape:

 .word 0                       # 0x0 Début Alien Crane
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0 fin l1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0 fin l2
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0 fin l3
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0 fin l4
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0 fin l5
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0 fin l6
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0 fin l7
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0 debut Alien Hélico
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0 fin l1
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0 fin l2
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1 fin l3
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 fin l4
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1 fin l5
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0 fin l6
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0 fin l7
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x1 Fin Alien 2
 .word 0                       # 0x0 Premier pixel Alien Spray
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0 fin l1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0 fin l2
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x0 fin l3
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0 fin l4
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x1 fin l5
 .word 0                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x1 fin l6
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x1 fin l7
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0
 .word 0                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x1
 .word 1                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x0 fin alien spray


# La position horizontale du coin supérieur gauche des bâtiments
  .data
 .align 2
buildingPosX:
 .word 19                      # 0x13
 .word 46                      # 0x2e
 .word 73                      # 0x49
 .word 100                     # 0x64

# La position verticale du sommet des batiments
.globl buildingPosY
 .align 2
buildingPosY:
 .word 49

# La position horizontale du coin supérieur gauche du canon du joueur.
 .data
 .align 2
shooterPosX:
 .word 58                      # 0x3a

# La forme du joueur (Tableau [5][13])
# ** Modifier si vous avez du style, sinon laissez cela aux professionnels **
  .data
 .align 2
shooter:
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 # Modifié lorsqu'il se fait toucher pour la 2eme fois
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 # Modifié lorsqu'il se fait toucher pour la 2eme fois
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 0                       # 0x0
 .word 1                       # 0x1
 .word 1                       # 0x1 # Modifié lorsqu'il se fait toucher pour la 1ere fois
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 # Modifié lorsqu'il se fait toucher pour la 2eme fois
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 # Modifié lorsqu'il se fait toucher pour la 2eme fois
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 # Modifié lorsqu'il se fait toucher pour la 1ere fois
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 # Modifié lorsqu'il se fait toucher pour la 1ere fois
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1
 .word 1                       # 0x1 # Modifié lorsqu'il se fait toucher pour la 1ere fois
 .word 1                       # 0x1
 .word 1                       # 0x1

# Table branchement du switch
# ** Ne pas modifier **
  .data
 .align 2
$switch.table.keyStroke:
 .word 1                       # 0x1
 .word 3                       # 0x3
 .word 2                       # 0x2

################################################################################
# Partie .text (le programme)
#
# La boucle de jeu.
# Ici c'est à vous d'ajouter les fonctions pour détecter la fin de partie et de remplir la boucle de jeu
#
################################################################################

.text

 .globl main                    # -- Begin function main
main:                                   # @main

# Quelques initialisations
# ** Ne pas modifier **
 addiu $sp, $sp, -24
 sw $ra, 20($sp)
 sw $fp, 16($sp)
 move $fp, $sp
 lui $at, %hi(enemiesPerRow)
 lw $at, %lo(enemiesPerRow)($at)
 sll $v0, $at, 4
 subu $at, $v0, $at
 lui $v0, %hi(boxTopPosX)
 lw $v0, %lo(boxTopPosX)($v0)
 addu $at, $at, $v0
 addiu $at, $at, -4
 lui $v0, %hi(enemiesRows)
 lw $v0, %lo(enemiesRows)($v0)
 sll $v1, $v0, 1
 sll $v0, $v0, 3
 lui $a0, %hi(boxSizeX)
 sw $at, %lo(boxSizeX)($a0)
 addu $at, $v0, $v1
 lui $v0, %hi(boxTopPosY)
 lw $v0, %lo(boxTopPosY)($v0)
 addu $at, $at, $v0
 addiu $at, $at, -3
 lui $v0, %hi(boxSizeY)
 sw $at, %lo(boxSizeY)($v0)
 jal clean_screen


# À partir d'ici, c'est à vous de jouer :
 jal Affiche_Invaders_Start		#Affiche le fond de l'écran de Démarrage
 jal Message_Depart			#Affiche les Textes sur l'écran de Démarrage
 li $a0 250
 jal sleep
 jal musique_ecran_titre		#Joue la Musique de départ
Démarrage_du_jeu:
 jal clean_screen
 jal Menu_difficulté			#Affiche le Menu de Difficulté et permet au joueur d'en choisir une
 move $s7 $v0
 move $a0 $s7
 jal initialiser_le_jeu			#Initialise les Variables liées à la difficulté choisie
 jal clean_screen

 #Queqlues Initialisations de la boucle de jeu
 li $s0 0  #Tour actuel
 lw $s1 Variable_Vitesse #Nombre de tours sauté pour les aliens
 addi $s1 $s1 7
 li $s2 0  #score à perdre par le temps
 li $s3 0  #Compteur pour limiter les tirs du joueur
 li $s4 0  #frame d'invincibilité
 li $s5 0  #etat de l'alien spray

boucle_jeu:

 # Fait bouger + tirer les extraterrestres lorsque le compteur de tour est congru a 0 mod $s1(Vitesse Aliens)
 move $a0 $s0
 move $a1 $s1
 li $t0 2
 div $a1 $a1 $t0
 jal modulo
 bgt $v0 $zero sauter_extra
 jal verif_colone_vide		#met à jour la taille en X de la Box Aliens
 jal alienTurn			#Fait bouger+Tirer les Aliens
 move $a0 $s5
 jal change_forme_alien		#Change la Forme des Aliens pour leurs Prochain déplacement
 move $s5 $v0
 jal Clean_Pixel_bug		#retire un Pixel restant après déplacement
 sauter_extra:

 # Récupère l'entrée et fais agir le joueur (déplace ou tire) en conséquence
 #move $a0 $s0
 #li $a1 2
 #jal modulo
 #bgt $v0 $zero sauter_joueur

 beqz $s3  Saut_decrement_limiteur	#Decrémente le compteur de Cooldown si besoin
 subi $s3 $s3 1
 Saut_decrement_limiteur:

 jal keyStroke				#Prend l'entrée du joueur
 li $t0 3

   move $t0 $v0


   li $t1 3
   bne $t1 $v0 Saut_limiteur 		 #Si c'est pas un Tir Saute au tour classique du joueur
   bnez $s3 saut_tir			#Sinon test si le joueur est en Cooldown
   lw $s3 Cooldown			#Joue un son et inititialise le compteur sinon
   #Son Tir
   li $a0 80
   li $a1 50
   li $a2 8
   li $a3 60
   li $v0 33
   syscall

   j Saut_limiteur
   saut_tir:				#Initialise le compteur et saute le tour du joueur
   li $t0 0
   Saut_limiteur:
   move $a0 $t0
   jal printShooter

 sauter_joueur:

 # Résolution des collisions


 jal resolveAndPrintShots

 beqz $s4 saut_decrement_invulnerabilite	#decremente le compteur d'invulnérabilité si besoin
 subi $s4 $s4 1
 saut_decrement_invulnerabilite:

 move $a0 $s4					#Perte d'une Vie si le joueur est touché
 jal perdreVie
 move $s4 $v0


 # Affiche les batiments
 jal printBuilding

 #Perte de point tous les 10 tours
 li $a1 10
 move $a0 $s0
 jal modulo
 bgtz $v0 Sauter_perte_point
 addi $s2 $s2 1
Sauter_perte_point:


 # Teste la fin de la partie
 jal verif_defaite_extermination
 jal verif_defaite_invasion
 lw $a0 has_lost
 beq $a0 1 a_perdu
 jal verif_gagne			#renvoie le nombre d'alien encore en vie


#Met à jour la vitesse des aliens en fonction du nombre d'aliens en vie
 move $s1 $v0
 li $t0 2
 div $s1 $s1 $t0
 lw $t0 Variable_Vitesse
 add $s1 $s1 $t0

#teste la victoire
 beq $v0 0 a_gagne #la il a gagné !!


 # Incrémentation du compteur
 addi $s0 $s0 1


 # Ralentis le ju pour permettre à des humains de gagner... enfin pas toujours...
 li $a0 10
 jal sleep


 j boucle_jeu



# Ajoutez vos fonctions en dessous :



#Print_Carre
#print un carré 4*4 de couleur a0 en position $a1, frameBuffer deja placé
Affiche_carre:
#prologue

#corps
 li $t0 6
 li $t1 36
 boucle_Affiche_carre:
   sw $a0 0($a1)			#print le pixel et decremente les compteur
   subi $t0 $t0 1
   subi $t1 $t1 1
   addi $a1 $a1 4
   bgtz $t0 boucle_Affiche_carre	#reinitialise le compteur et retourne à la ligne
   li $t0 6
   addi $a1 $a1 488
   bgtz $t1 boucle_Affiche_carre

#epilogue
 jr $ra


#afficher ligne affiche une ligne de 16 places représenté sur une surface 4*4 par place
#entrées : $a0 vaut la couleur à afficher
#	   $a1 vaut le frameBuffer déjà placé
#	   $a2 vaut le nombre représentant la ligne à afficher
Afficher_Ligne:

#Prologue
  addiu $sp $sp -20
  sw $ra 0($sp)
  sw $s0 4($sp)
  sw $s1 8($sp)
  sw $s2 12($sp)
  sw $s3 16($sp)

#corps
  move $s0 $a0
  move $s1 $a1
  addi $s2 $s1 368				#Place un frameBuffer sur le pixel Haut/gauche du dernier Carré de la Ligne
  move $s3 $a2

  boucle_affiche_ligne:
    blt $s2 $s1 fin_affiche_ligne
    move $a0 $s3				#trouver si le dernier bit est 1 ou 0
    li $a1 2
    jal modulo
    beqz $v0 boucle_affiche_ligne_part_2	# afficher le carré si c'est un 1
    move $a0 $s0
    move $a1 $s2
    jal Affiche_carre
    boucle_affiche_ligne_part_2:		#increments
    subi $s2 $s2 24
    li $t0 2
    div $s3 $s3 $t0
    j boucle_affiche_ligne

  fin_affiche_ligne:
#epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  lw $s2 12($sp)
  lw $s3 16($sp)
  addi $sp $sp 20
  jr $ra


#affiche Invaders_Start
#Affiche le space invader pour l'écran titre
Affiche_Invaders_Start:

#prologue
  addiu $sp $sp -20
  sw $ra 0($sp)
  sw $s0 4($sp)
  sw $s1 8($sp)
  sw $s2 12($sp)
  sw $s3 16($sp)

#corps
  la $s0 frameBuffer
  addi $s0 $s0 3132				#Place le FrameBuffer au bon emplacement
  la $s1 Tableau_Invaders_Start
  li $s2 8
  la $s3 buildingColor
  lw $s3 0($s3)					#Load la couleur dans $s3

  boucle_Affiche_Invaders_Start:
    lw $a2 0($s1) 				#entrée des paramètres pour afficher une ligne
    move $a0 $s3
    move $a1 $s0
    jal Afficher_Ligne
    subi $s2 $s2 1				#incrémentation des compteurs et position
    addi $s0 $s0 3072
    addi $s1 $s1 4
    bgtz $s2 boucle_Affiche_Invaders_Start

#epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  lw $s2 12($sp)
  lw $s3 16($sp)
  addi $sp $sp 20
  jr $ra


#Affiche_ligne_Quadrillé
# entrée :	$a0 prend le tableau frameBuffer bien placé
#		$a1 prend la taille
#		$a2 prend la couleur
Affiche_Ligne_Quadrille:
#corps

  li $t1 2
  div $a1 $a1 $t1			#Print 2 carré par 2 donc divise le nombre à afficher par 2
  boucle_affiche_ligne_quadrille:
    blez $a1 fin_affiche_ligne_quadrille
    sw $a2 0($a0)			#Print les 2 carré en diagonales
    sw $a2 516($a0)
    addi $a0 $a0 8			#incrémente compteur et frameBuffer
    subi $a1 $a1 1
    j boucle_affiche_ligne_quadrille

  fin_affiche_ligne_quadrille:
    jr $ra


#Affiche_Colonne_Quadrillé
# entrée :	$a0 prend la position en x
#		$a1 prend la position en y
#		$a2 prend la taille
#		$a3 prend la couleur
Affiche_Colonne_Quadrille:
#corps

  li $t0 2
  div $a1 $a1 $t0			#Print 2 carré par 2 donc divise le nombre à afficher par 2
  boucle_affiche_colonne_quadrille:
    blez $a1 fin_affiche_colonne_quadrille
    sw $a2 0($a0)			#Print les 2 carré en diagonales
    sw $a2 516($a0)
    addi $a0 $a0 1024			#Incrémente le compteur et le frameBuffer
    subi $a1 $a1 1
    j boucle_affiche_colonne_quadrille

  fin_affiche_colonne_quadrille:
    jr $ra


#Affiche cadre quadrillé
#entrée :	$a0 FrameBufffer bien placé
#		$a1 La taille en x
#		$a2 La taille en y
#		$a3 La couleur
Affiche_Cadre_Quadrille:
#Prologue
  addiu $sp $sp -20
  sw $ra 0($sp)
  sw $s0 4($sp)
  sw $s1 8($sp)
  sw $s2 12($sp)
  sw $s3 16($sp)

#corps
  move $s0 $a0
  move $s1 $a1
  move $s2 $a2
  move $s3 $a3

  move $a2 $s3				#Affiche la ligne du haut
  jal Affiche_Ligne_Quadrille

  move $a0 $s0				#Affiche la colonne de gauche
  move $a1 $s2
  move $a2 $s3
  jal Affiche_Colonne_Quadrille

  li $t0 512				#Affiche la ligne du bas
  move $t1 $s2
  subi $t1 $t1 1
  mul $t0 $t0 $t1
  add $a0 $s0 $t0
  move $a1 $s1
  move $a2 $s3
  jal Affiche_Ligne_Quadrille

  li $t0 4				#Affiche la colonne de droite
  move $t1 $s1
  subi $t1 $t1 1
  mul $t0 $t0 $t1
  add $a0 $s0 $t0
  move $a1 $s2
  addi $a1 $a1 2
  move $a2 $s3
  jal Affiche_Colonne_Quadrille

#epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  lw $s2 12($sp)
  lw $s3 16($sp)
  addi $sp $sp 20
  jr $ra


#Clean_pixel_bug
#Clean_pixel_bug sert à clean la ligne au dessus des aliens ou un pixel apparait lors du changement de ligne des aliens
Clean_Pixel_bug:
#prologue:
  addiu $sp $sp -4
  sw $ra 0($sp)

#corps
  li $a0 0					#Initialise les variables pour clean la ligne du dessus de la Box des Aliens
  lw $a1 boxTopPosY
  subi $a1 $a1 2
  bltz $a1 fin_Clean_Pixel_bug
  li $a2 128
  li $a3 1
  jal cleanPartOfScreen
fin_Clean_Pixel_bug:

#épilogue
  lw $ra 0($sp)
  addi $sp $sp 4
  jr $ra


#Attend que le joueur choissise 5, 4 et 6 servant à changer la difficulté
#renvoie 0 1 2 pour la difficultée dans v0
Menu_difficulté:
  #prologue
  addiu $sp $sp -8
  sw $ra 0($sp)
  sw $s0 4($sp)

  #corps
  li $s0 1					#Affiche le Menu de Difficulté et le curseur, initialise les vraibles
  jal affiche_Menu_Difficulté			#et supprime la pile de keystroke
  jal keyStroke
  move $a0 $s0
  jal Curseur_Menu
  boucle_Menu_difficulté:
    li $a0 25
    jal sleep
    jal keyStroke				#Prend une entrée du joueur
    li $t0 3
    beq $v0 $t0 fin_Menu_difficulté		#test l'entrée et agit jump en fonction
    li $t0 1
    beq $v0 $t0 press_4_Menu_difficulté
    li $t0 2
    beq $v0 $t0 press_6_Menu_difficulté
    j boucle_Menu_difficulté

    press_4_Menu_difficulté:			#Si le joueur press 4 Joue un Son et modifie la valeur de difficulté
      #Son
      li $a0 80
      li $a1 150
      li $a2 112
      li $a3 80
      li $v0 33
      syscall

      subi $s0 $s0 1
      move $a0 $s0				#Change le compteur par son Modulo 3 pour le garder entre 0 et 2
      li $a1 3
      jal modulo
      move $s0 $v0
      move $a0 $s0
      jal Curseur_Menu				#Affiche le Curseur du Menu selon la valeur actuel de $s0

      j boucle_Menu_difficulté

    press_6_Menu_difficulté:			#Si le joueur press 6 Joue un Son et modifie la valeur de difficulté
      #Son
      li $a0 80
      li $a1 150
      li $a2 112
      li $a3 80
      li $v0 33
      syscall

      addi $s0 $s0 1
      move $a0 $s0				#Change le compteur par son Modulo 3 pour le garder entre 0 et 2
      li $a1 3
      jal modulo
      move $s0 $v0
      move $a0 $s0
      jal Curseur_Menu				#Affiche le Curseur du Menu selon la valeur actuel de $s0
      j boucle_Menu_difficulté

  fin_Menu_difficulté:				#renvoie le compteur pour la difficultée et joue un son de validation
    #Son validation
    li $a0 80
    li $a1 150
    li $a2 112
    li $a3 80
    li $v0 33
    syscall
    addi $a0 $a0 7
    addi $a1 $a1 50
    syscall

    move $v0 $s0

   #epilogue
    lw $ra 0($sp)
    lw $s0 4($sp)
    addi $sp $sp 8
    jr $ra


#Attend que le joueur choissise 5, 4 et 6 servant à changer le curseur
#renvoie 0/1 pour Rejouer/quitter dans $v0
Menu_Fin:
  #prologue
  addiu $sp $sp -8
  sw $ra 0($sp)
  sw $s0 4($sp)

  #corps
  li $s0 1
  jal affiche_Menu_Fin			#Affiche le Menu de fin et supprime la pile de keyStroke
  move $a0 $s0
  jal Curseur_Menu_Fin
  jal keyStroke
  boucle_Menu_Fin:
    li $a0 25
    jal sleep				#Petit Sleep entre chaque tour de boucle pour pas surcharger l'écran
    jal keyStroke			#Prend l'entrée du joueur
    li $t0 3
    beq $v0 $t0 fin_Menu_Fin		#test l'entrée et jump en fonction
    li $t0 1
    beq $v0 $t0 press_4_Menu_Fin
    li $t0 2
    beq $v0 $t0 press_6_Menu_Fin
    j boucle_Menu_Fin

    press_4_Menu_Fin:			#Si le joueur press 4 Joue un Son et modifie la valeur de $s0
      #Son
      li $a0 80
      li $a1 150
      li $a2 112
      li $a3 80
      li $v0 33
      syscall

      subi $s0 $s0 1
      move $a0 $s0			#Change le compteur par son Modulo 2 pour le garder entre 0 et 1
      li $a1 2
      jal modulo
      move $s0 $v0
      move $a0 $s0
      jal Curseur_Menu_Fin		#Affiche le Curseur du Menu selon la valeur actuel de $s0
      j boucle_Menu_Fin

    press_6_Menu_Fin:			#Si le joueur press 6 Joue un Son et modifie la valeur de $s0
      #Son
      li $a0 80
      li $a1 150
      li $a2 112
      li $a3 80
      li $v0 33
      syscall

      addi $s0 $s0 1
      move $a0 $s0			#Change le compteur par son Modulo 2 pour le garder entre 0 et 1
      li $a1 2
      jal modulo
      move $s0 $v0
      move $a0 $s0
      jal Curseur_Menu_Fin		#Affiche le Curseur du Menu selon la valeur actuel de $s0
      j boucle_Menu_Fin

  fin_Menu_Fin:				#renvoie le compteur mod 2 pour Rejouer/Quitter
    #Son validation
    li $a0 80
    li $a1 150
    li $a2 112
    li $a3 80
    li $v0 33
    syscall
    addi $a0 $a0 7
    addi $a1 $a1 50
    syscall

    move $v0 $s0

   #epilogue
    lw $ra 0($sp)
    lw $s0 4($sp)
    addi $sp $sp 8
    jr $ra


#Initialise les variables du jeu suivant la difficulté choisie
#arguments : $a0:Difficulté (0:Facile, 1:Normal[Défaut], 2:Difficile)
initialiser_le_jeu:
  # Prologue
  addi $sp $sp -4
  sw $ra 0($sp)

# Corps
  li $t0 2
  beq $a0 $t0 version_difficile
  li $t0 0
  beq $a0 $t0 version_facile

  #sinon normal

  #Change les cooldown entre les tirs
  lw $t0 CooldownNormal
  sw $t0 Cooldown

  #Change nombre vies
  lw $t1 players_life_normal
  sw $t1 players_life

  #Change vitesse aliens
  lw $t1 Variable_Vitesse_normal
  sw $t1 Variable_Vitesse

  #Modifie Points de base
  lw $t1 PointsBaseNormal
  sw $t1 PointsBase

  #Modifie Points par vie restantes
  lw $t1 PointsVieRestantesNormal
  sw $t1 PointsVieRestantes

  #Modifie Points par alien vivants
  lw $t1 CoutPointsAliensVivantsNormal
  sw $t1 CoutPointsAliensVivants

  #Modifie Points par pixel de batiment
  lw $t1 PointsPixelsBatimentsNormal
  sw $t1 PointsPixelsBatiments

  #Change rayon explosion tirs
  lw $t1 shotRadiusBuildingNormal
  sw $t1 shotRadiusBuilding

  #Change le fichier de sauvegarde des scores
  la $a0 score_normal
  la $a1 filename
  jal Copie

  j fin_initialisation

  #facile
 version_facile:

  #Change les cooldown entre les tirs
  lw $t0 CooldownFacile
  sw $t0 Cooldown

  #Change nombre vies
  lw $t1 players_life_easy
  sw $t1 players_life

  #Change vitesse aliens
  lw $t1 Variable_Vitesse_facile
  sw $t1 Variable_Vitesse

  #Modifie Points de base
  lw $t1 PointsBaseFacile
  sw $t1 PointsBase

  #Modifie Points par vie restantes
  lw $t1 PointsVieRestantesFacile
  sw $t1 PointsVieRestantes

  #Modifie Points par alien vivants
  lw $t1 CoutPointsAliensVivantsFacile
  sw $t1 CoutPointsAliensVivants

  #Modifie Points par pixel de batiment
  lw $t1 PointsPixelsBatimentsFacile
  sw $t1 PointsPixelsBatiments

  #Change rayon explosion tirs
  lw $t1 shotRadiusBuildingNormal
  sw $t1 shotRadiusBuilding

  #Change le fichier de sauvegarde des scores
  la $a0 score_facile
  la $a1 filename
  jal Copie

  j fin_initialisation

 version_difficile:

 #Change les cooldown entre les tirs
 lw $t0 CooldownDifficile
 sw $t0 Cooldown

 #Change nombre vies
 lw $t1 players_life_hard
 sw $t1 players_life

 #Change rayon explosion tirs
 lw $t1 shotRadiusBuildingHard
 sw $t1 shotRadiusBuilding

 #Change vitesse aliens
 lw $t1 Variable_Vitesse_difficile
 sw $t1 Variable_Vitesse

 #Modifie Points de base
 lw $t1 PointsBaseDifficile
 sw $t1 PointsBase

 #Modifie Points par vie restantes
 lw $t1 PointsVieRestantesDifficile
 sw $t1 PointsVieRestantes

 #Modifie Points par alien vivants
 lw $t1 CoutPointsAliensVivantsDifficile
 sw $t1 CoutPointsAliensVivants

 #Modifie Points par pixel de batiment
 lw $t1 PointsPixelsBatimentsDifficile
 sw $t1 PointsPixelsBatiments

 #Change le fichier de sauvegarde des scores
 la $a0 score_difficile
 la $a1 filename
 jal Copie

 # Epilogue
 fin_initialisation:
 lw $ra 0($sp)
 addi $sp $sp 4
 jr $ra


# Copie un string
# arguments :
#       $a0: adresse du string à copier
#       $a1: adresse où copier le string
Copie:
  lbu $t0, 0($a0)
  sb $t0, 0($a1)
  addi $a0, $a0, 1
  addi $a1, $a1, 1
  beqz $t0, FinCopie # On s'arrete si on charge le caractère de fin de chaine
  j Copie
FinCopie:
  jr $ra

# Renvoi a0%a1+
modulo:

 #corps
 move $t0 $a0				#calcule Le modulo
 div $t0 $t0 $a1
 mul $t0 $t0 $a1
 subu $a0 $a0 $t0
 bge $a0 $zero fin_modulo		#Ajoute le modulo pour le faire passer en positif si il était négatif
   add $a0 $a0 $a1
 fin_modulo:
   move $v0 $a0

 #epilogue
 jr $ra


 #Renvoi le nombre d'ennemis en vie
 verif_gagne:
  la $t0 enemiesLife
  li $t1 0
  li $t2 15			#intialise le tableau et le compteru
  boucle_verif_gagne:
  ble $t2 $zero retour_gagner	#Quitte après 15 tours
  lw $t3 0($t0)			#ajoute 1 si la valeur dans le tableau vaut 1 (Vivant) sinon ajoute 0 (mort)
  add $t1 $t1 $t3
  addi $t0 $t0 4		#Incrémente pointeur sur tableau et compteur
  subi $t2 $t2 1
  j boucle_verif_gagne

  retour_gagner:
    move $v0 $t1
    jr $ra


#modifie has_lost si tous les batients détruits
#retourne le nombre de pixels de batiment restant dans v0
verif_defaite_extermination:
  la $t0 building				#Initialise le compteur de pixel,Compteur $t2 et adresse tableau batiments
  li $t1 0
  li $t2 191
  boucle_verif_defaite_extermination:
  blez $t2 fin_verif_extermination		#Ajoute à $t2 la valeur du pixel étudié 1/0 pour pixel vivant/détruit
  lw $t3 0($t0)
  add $t1 $t1 $t3
  addi $t0 $t0 4				#Incrémente les compteurs
  subi $t2 $t2 1
  j boucle_verif_defaite_extermination

  fin_verif_extermination:		#met 1 dans has lost si tous les batiments détruits
  bgtz $t1 fin_extermination
  li $t0 1
  sw $t0 has_lost

  fin_extermination:			#retourne le nombre de pixels restants
  move $v0 $t1
  jr $ra


# Modifie players_life si le joueur s'est fait touché et remet has_lost à 0.
# Laisse has_lost à 1 si le joueur n'a plus de vie.
# entrée : $a0 prend 0 sauf si le joueur est en frame d'invulnérabilité
perdreVie:
  addiu $sp, $sp, -8
  sw $ra, 0($sp)
  sw $s0 4($sp)

  lw $t0 has_lost
  li $t1 1
  move $s0 $a0
  blt $t0 $t1 fin_perdre_vie

  #si le joueur s'est fait touché, retirer une vie Sauf si invulnérable
  lw $t2 players_life
  bnez $a0 Invulnérabilité
  subi $t2 $t2 1
  sw $t2 players_life
  lw $s0 temps_invulnérabilité

  #Son lorsque touché
  li $a0 50
  li $a1 1000
  li $a2 116
  li $a3 120
  li $v0 31
  syscall

  jal change_forme_joueur

  la $a0 message_touche	 #Message dans le terminal
  jal print_string

  Invulnérabilité:
  lw $t2 players_life
  blez $t2 fin_perdre_vie #Si il lui en reste encore, remmettre has_lost à 0
  sw $zero has_lost

 fin_perdre_vie:
  move $v0 $s0		#Renvoie le nouveau temps d'invulnérabilité ( Souvent le même qu'au départ)
  lw $ra, 0($sp)
  lw $s0, 4($sp)
  addiu $sp, $sp, 8
  jr $ra


# Change la forme du joueur lorsqu'il se fait toucher
change_forme_joueur:

  #corps
  la $t0 shooter
  lw $t1 160($t0)
  li $t2 1
  blt $t1 $t2 change_deuxieme_forme
   #Ne s'est pas encore fait touché
   sw $zero 160($t0)
   sw $zero 200($t0)
   sw $zero 216($t0)
   sw $zero 248($t0)
   j fin_change_forme_joueur

   #Le joueur s'était déjà fait touché
   change_deuxieme_forme:
   sw $zero 116($t0)
   sw $zero 140($t0)
   sw $zero 172($t0)
   sw $zero 188($t0)

  #epilogue
  fin_change_forme_joueur:
  jr $ra


#modifie has_lost si colision alien/batiments

verif_defaite_invasion:
#prologue
  addiu $sp, $sp, -4
  sw $ra, 0($sp)

#corps
  jal Ligne_min_aliens
  lw $t0 buildingPosY
  blt $v0 $t0 fin_defaite_invasion	#Si l'alien le plus bas est sur la même ligne que les batiments, met 1 dans has_lost
  li $t0 1
  sw $t0 has_lost

#epilogue
 fin_defaite_invasion:
  lw $ra, 0($sp)
  addiu $sp, $sp, 4
  jr $ra


#renvoie la ligne la plus basse où un alien est présent
#Précondition : il reste au moins 1 alien en vie
#effet de bord: modifie boxSizeY si dernière ligne des aliens est morte
Ligne_min_aliens:

#prologue
  addiu $sp, $sp, -8
  sw $ra 0($sp)
  sw $s0 4($sp)

#Corps
  la $t0 enemiesLife		#Initialise un poiteur vers la fin du tableau de vie des Aliens
  addi $t0 $t0 56
  li $t1 0
  li $s0 15			#Permet de trouver la Position du dernier alien
 boucle_Ligne_min_aliens:
  bgtz $t1 fin_Ligne_min_aliens		#Parcourt le tableau en sens inverse
  lw $t3 0($t0)				#Quitte dès qu'un Alien Vivant est trouvé
  move $t1 $t3
  subi $t0 $t0 4
  subi $s0 $s0 1
  j boucle_Ligne_min_aliens

 fin_Ligne_min_aliens:
  move $a0 $s0
  jal ReduitBoxEnemyY			#Réduit la taille de la Box ennemi en x si Besoin
  div $s0 $s0 5				#Renvoie La position Y de La box ennemie
  addi $s0 $s0 1
  move $a0 $s0
  jal enemyBoxPosY

#Epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  addiu $sp, $sp, 8
  jr $ra


#Réduit la taille de la box des aliens si les plus bas sont tous morts
#a0 prend la position du dernier alien vivant, allant de 0 à 14
ReduitBoxEnemyY:

#Corps
 lw $t1 boxSizeY
 li $t0 9
 bgt $a0 $t0 FinReduitBoxEnemyY		#Rentre dans $t0 la taille voulu en fonction de la position du dernier ennemi vivant
 li $t1 18
 li $t0 4
 bgt $a0 $t0 FinReduitBoxEnemyY
 li $t1 9


#Epilogue
FinReduitBoxEnemyY:
 sw $t1 boxSizeY			#Enregistre la nouvelle taille
 jr $ra


 #verifie si une collone doit être détruite et appel sa destruction si besoin
 #Pas d'entrée ni de sortie
 #effet de bord: destruction des colones vides
 verif_colone_vide:
 #prologue
  addiu $sp $sp -8
  sw $ra 0($sp)
  sw $s0 4($sp)
#corps
  la $s0 enemiesLife				#met la somme des vies des 3 ennemies de gauche dans $t2
  lw $t1 0($s0)
  move $t2 $t1
  lw $t1 20($s0)
  add $t2 $t2 $t1
  lw $t1 40($s0)
  add $t2 $t2 $t1

  bgt $t2 $zero second_test_verif_colone_vide   #si les 3 ennemies de gauche morts : reduit box Alien par la gauche
  li $a0 0
  jal reduit_box_alien_X

 second_test_verif_colone_vide:
  lw $t2 enemiesPerRow			#met dans $t3 la somme des vies des aliens de droite de la box actuelle
  subi $t2 $t2 1
  li $t1 4
  mul $t2 $t1 $t2
  add $s0 $s0 $t2
  lw $t3 0($s0)
  addi $s0 $s0 20
  lw $t1 0($s0)
  add $t3 $t3 $t1
  addi $s0 $s0 20
  lw $t1 0($s0)
  add $t3 $t3 $t1

  bgt $t3 $zero fin_verif_colone_vide	#reduit la taille de la box par la droite si $t3=0
  li $a0 1
  jal reduit_box_alien_X

#Epilogue
 fin_verif_colone_vide:
  lw $ra, 0($sp)
  lw $s0 4($sp)
  addiu $sp $sp 8
  jr $ra


 #réduit la taille de la box des aliens et déplace le tableau si $a0 = 0
 #entrée $a0
 #sortie rien
 #effet de bord: change la taille de la bo alien en x et change le tableau de vie alien si besoin
 reduit_box_alien_X:
#Prologue
  addiu $sp $sp -4
  sw $ra 0($sp)

#Corps
  lw $t1 boxSizeX		#reduit la taille de la box ennemi en colonne
  subi $t1 $t1 14
  sw $t1 boxSizeX

  lw $t1 enemiesPerRow 			#reduit le nombre d'ennemies par ligne
  subi $t1 $t1 1
  sw $t1 enemiesPerRow

  bgt $a0 $zero fin_reduit_box_alien_X
  lw $t1 boxTopPosX			 #Déplace la boite si on réduit par la Gauche
  addi $t1 $t1 14
  sw $t1 boxTopPosX


  li $t8 14				#déplace les aliens pour aller avec le déplacement de boite si on suprime la ligne de gauche
  la $t0 enemiesLife
 boucle_reduit_box_alien:
  beq $t8 $zero fin_boucle_reduit_box_alien_X
  lw $t2 4($t0)
  sw $t2 0($t0)
  addi $t0 $t0 4
  subi $t8 $t8 1
  j boucle_reduit_box_alien
fin_boucle_reduit_box_alien_X:
  sw $zero 0($t0)			#Met 0 dans la vie du dernier Alien
  jal clean_screen

#Epilogue
fin_reduit_box_alien_X:
  lw $ra 0($sp)
  addi $sp $sp 4
  jr $ra


# Modifie la couleur des ennemis en fonction de la ligne
# arument : $a0 Type d'ennemi (0, 1 ou 2)
change_enemies_color:
#Prologue
  addiu $sp, $sp, -16
  sw $ra, 12($sp)
  sw $a0, 8($sp)
  sw $t1, 4($sp)		#Stoquage de $t0 $t1 car ajout d'un jal à un endroit non prévu pour dans le code Originel
  sw $t0, 0($sp)

#Corps
  li $t0 1
  li $t1 2

  beq $a0 $zero type1		#Jump selon le type d'ennemi
  beq $a0 $t0 type2
  beq $a0 $t1 type3
  # sinon
  li $t0 16777215 # 0xffffff
  j fin_change_enemies_color
 type1:				#Change la couleur en fonction du type d'ennemi
  lw $t0 enemyColor1
  j fin_change_enemies_color
 type2:
  lw $t0 enemyColor2
  j fin_change_enemies_color
 type3:
  lw $t0 enemyColor3
  j fin_change_enemies_color

 fin_change_enemies_color:
  la $t1 enemyColor
  sw $t0 0($t1)

#Epilogue
  lw $t0, 0($sp)
  lw $t1, 4($sp)
  lw $a0, 8($sp)
  lw $ra, 12($sp)
  addiu $sp, $sp, 16
  jr $ra


#max de 2 nombre
Minimum_2_valeurs:
   blt $a0 $a1 Minimum_2_valeurs_1
   move $v0 $a1
   j fin_Minimum_2_valeurs
  Minimum_2_valeurs_1:
   move $v0 $a0
  fin_Minimum_2_valeurs:
   jr $ra


#affichage d'un tableau 5*5 en couleur
#a0 prend une valeur representant l'affichage voulu
#a1 prend un pointeur sur la couleur voulu
#$a2 prend la table d'affichage bien placée
#effet de bord : affichage du message lié a la valeur
affiche_tableau:
 #prologue
 addiu $sp $sp -24
 sw $s4 20($sp)
 sw $s3 16($sp)
 sw $s2 12($sp)
 sw $s1 8($sp)
 sw $s0 4($sp)
 sw $ra 0($sp)

 #Corps
 move $s0 $a2
 addi $s0 $s0 2064		#Initialisations
 lw $s1 0($a1)
 li $s2 5
 li $s3 25
 move $s4 $a0

boucle_affiche_tableau:
 move $a0 $s4					#Change la couleur du pixel si le dernier bit est 1
 li $a1 2
 jal modulo
 beqz $v0 test_affiche_tableau
 sw $s1 0($s0)

test_affiche_tableau:
 li $t0 2					#Increments basiques
 div $s4 $s4 $t0
 subi $s0 $s0 4
 subi $s2 $s2 1
 subi $s3 $s3 1

 bgtz $s2 boucle_affiche_tableau		#Incréments Si changement de ligne
 subi $s0 $s0 492
 li $s2 5
 beqz $s3 fin_affiche_tableau			#Quitte si les 25 pixels sont faits
 j boucle_affiche_tableau
fin_affiche_tableau:

#epilogue
 lw $s4 20($sp)
 lw $s3 16($sp)
 lw $s2 12($sp)
 lw $s1 8($sp)
 lw $s0 4($sp)
 lw $ra 0($sp)
 addiu $sp $sp 24
 jr $ra


#Messages_Depart
Message_Depart:
  #prologue
     addiu $sp $sp -16
     sw $ra 0($sp)
     sw $s0 4($sp)
     sw $s1 8($sp)
     sw $s2 12($sp)

  #corps
    la $s0 frameBuffer
    addi $s0 $s0 7388
    la $s1 shooterColor
    la $s2 Alphabet

    lw $a0 48($s2)		#M
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 0($s2)		#A
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 72($s2)		#S
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 4504


    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 52($s2)		#N
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 84($s2)		#V
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 0($s2)		#A
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 12($s2)		#D
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 72($s2)		#S
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    la $s0 frameBuffer
    addi $s0 $s0 29192
    la $s1 enemyColor2

    lw $a0 4($s2)		#B
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 72($s2)		#S
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 52($s2)		#N
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 0($s2)		#A
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 80($s2)		#U
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 44($s2)		#L
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 76($s2)		#T
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau


    addi $s0 $s0 244


    lw $a0 100($s2)		#Z
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 4($s2)		#B
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 4($s2)		#B
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau



  #epilogue
     lw $ra 0($sp)
     lw $s0 4($sp)
     lw $s1 8($sp)
     lw $s2 12($sp)
     addiu $sp $sp 16
     jr $ra



#Affiche le Menu du jeu
affiche_Menu_Difficulté:
  #prologue
     addiu $sp $sp -16
     sw $ra 0($sp)
     sw $s0 4($sp)
     sw $s1 8($sp)
     sw $s2 12($sp)

  #corps
    la $s0 frameBuffer
    addi $s0 $s0 6816
    la $s1 Couleur_Curseur
    la $s2 Alphabet

    la $a0 frameBuffer			#Affiche cadre
    addi $a0 $a0 1548
    li $a1 121
    li $a2 57
    lw $a3 buildingColor
    jal Affiche_Cadre_Quadrille

    lw $a0 12($s2)		#D
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 20($s2)		#F
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 20($s2)		#F
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 8($s2)		#C
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 80($s2)		#U
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 44($s2)		#L
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 76($s2)		#T
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 6520		#changement de mot
    la $s1 buildingColor

    lw $a0 20($s2)		#F
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 0($s2)		#A
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 8($s2)		#C
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 44($s2)		#L
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 5020		#changement de mot
    la $s1 Couleur_Normal

    lw $a0 52($s2)		#N
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 56($s2)		#O
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 48($s2)		#M
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 0($s2)		#A
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 44($s2)		#L
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 4992		#changement de ligne
    la $s1 Couleur_Message_Victoire

    lw $a0 12($s2)		#D
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 20($s2)		#F
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 20($s2)		#F
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 8($s2)		#C
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 44($s2)		#L
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    #epilogue
     lw $ra 0($sp)
     lw $s0 4($sp)
     lw $s1 8($sp)
     lw $s2 12($sp)
     addiu $sp $sp 16
     jr $ra

#Affiche le Menu du jeu
affiche_Menu_Fin:
  #prologue
     addiu $sp $sp -16
     sw $ra 0($sp)
     sw $s0 4($sp)
     sw $s1 8($sp)
     sw $s2 12($sp)

  #corps
    la $s0 frameBuffer
    addi $s0 $s0 10428
    la $s1 Couleur_Curseur
    la $s2 Alphabet

    la $a0 frameBuffer			#Affiche cadre
    addi $a0 $a0 1548
    li $a1 121
    li $a2 57
    lw $a3 buildingColor
    jal Affiche_Cadre_Quadrille

    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 36($s2)		#j
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 56($s2)		#O
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 80($s2)		#U
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 7556


    lw $a0 64($s2)		#Q
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 80($s2)		#U
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 76($s2)		#T
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 76($s2)		#T
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau



    #epilogue
     lw $ra 0($sp)
     lw $s0 4($sp)
     lw $s1 8($sp)
     lw $s2 12($sp)
     addiu $sp $sp 16
     jr $ra

#Curseur devant la difficutlé et clean du précédant curseur
#entrée $a0 prend la difficulté actuelle : 0/1/2 pour facile/normal/difficile
Curseur_Menu:

  #epilogue
  addiu $sp $sp -12
  sw $ra 0($sp)
  sw $s0 4($sp)
  sw $s1 8($sp)
  #corps
   move $s1 $a0

   li $a0 32				#clean l'ancien curseur
   li $a1 26
   li $a2 3
   li $a3 25
   jal cleanPartOfScreen

   la $s0 frameBuffer
   addi $s0 $s0 13440

   la $t0 Couleur_Curseur
   beqz $s1 Print_Curseur_Menu
   addi $s0 $s0 5120			#Place le curseur devant Normal

   li $t1 1
   beq $s1 $t1 Print_Curseur_Menu
   addi $s0 $s0 5120			#Place le curseur devant Difficile

   Print_Curseur_Menu:			#Print les pixels du curseur à l'emplacement précédement modifié
    lw $t0 0($t0)
    sw $t0 0($s0)
    addi $s0 $s0 512
    sw $t0 0($s0)
    addi $s0 $s0 4
    sw $t0 0($s0)
    addi $s0 $s0 508
    sw $t0 0($s0)
    addi $s0 $s0 4
    sw $t0 0($s0)
    addi $s0 $s0 4
    sw $t0 0($s0)
    addi $s0 $s0 504
    sw $t0 0($s0)
    addi $s0 $s0 4
    sw $t0 0($s0)
    addi $s0 $s0 508
    sw $t0 0($s0)

  #epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  addi $sp $sp 12
  jr $ra


#Curseur devant la difficutlé et clean du précédant curseur
#entrée $a0 prend la difficulté actuelle : 0/1 pour Rejouer/quitter
Curseur_Menu_Fin:

  #epilogue
  addiu $sp $sp -12
  sw $ra 0($sp)
  sw $s0 4($sp)
  sw $s1 8($sp)
  #corps
   move $s1 $a0

   li $a0 32				#clean l'ancien curseur
   li $a1 20
   li $a2 3
   li $a3 31
   jal cleanPartOfScreen

   la $s0 frameBuffer
   addi $s0 $s0 10368

   la $t0 Couleur_Curseur
   beqz $s1 Print_Curseur_Menu_Fin
   addi $s0 $s0 7680			#Place le curseur devant Quitter

   Print_Curseur_Menu_Fin:		#Print le Curseur
    lw $t0 0($t0)
    sw $t0 0($s0)
    addi $s0 $s0 512
    sw $t0 0($s0)
    addi $s0 $s0 4
    sw $t0 0($s0)
    addi $s0 $s0 508
    sw $t0 0($s0)
    addi $s0 $s0 4
    sw $t0 0($s0)
    addi $s0 $s0 4
    sw $t0 0($s0)
    addi $s0 $s0 504
    sw $t0 0($s0)
    addi $s0 $s0 4
    sw $t0 0($s0)
    addi $s0 $s0 508
    sw $t0 0($s0)

  #epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  addi $sp $sp 12
  jr $ra

#modifie les formes des aliens
#prend en entrée dans $a0 l'état de l'alien spray et renvoie son nouvel état
change_forme_alien:
#corps
  la $t0 enemies_shape


  #Fait touner les hélices de l'hélico 1
  lw $t1 384($t0)
  beqz $t1 Tourne_helice
  #Cas Hélce pleine
    li $t1 0
    sw $t1 384($t0)
    sw $t1 400($t0)
    sw $t1 412($t0)
    sw $t1 428($t0)
    j fin_Tourne_helice
  #Cas hélice touné
  Tourne_helice:
    li $t1 1
    sw $t1 384($t0)
    sw $t1 400($t0)
    sw $t1 412($t0)
    sw $t1 428($t0)

  fin_Tourne_helice:

  #Bouge alien bas
    lw $t1 312($t0)
  beqz $t1 Bouge_Machoire
  #Cas "machoire" bas
    li $t1 0
    sw $t1 308($t0)
    sw $t1 312($t0)
    li $t1 1
    sw $t1 352($t0)
    sw $t1 356($t0)
    sw $t1 360($t0)
    sw $t1 364($t0)
    j fin_Bouge_Machoire
  #Cas "machoire" haute
  Bouge_Machoire:
    li $t1 1
    sw $t1 308($t0)
    sw $t1 312($t0)
    li $t1 0
    sw $t1 352($t0)
    sw $t1 356($t0)
    sw $t1 360($t0)
    sw $t1 364($t0)

  fin_Bouge_Machoire:

  #Bouge l'Alien Spray
    beqz $a0 Etat_0_Spray
    li $t1 1
    beq $a0 $t1 Etat_1_Spray

   #Cas Etat_2 spray au milieu
    li $t1 1
    sw $t1 1104($t0)
    sw $t1 1112($t0)
    sw $t1 1120($t0)
    sw $t1 1132($t0)
    sw $t1 1140($t0)
    sw $t1 1148($t0)
    li $v0 0
    j Fin_Spray
    #Cas Etat_0 spray en bas
    Etat_0_Spray:
    li $t1 0
    sw $t1 1060($t0)
    sw $t1 1068($t0)
    sw $t1 1088($t0)
    sw $t1 1096($t0)
    sw $t1 1104($t0)
    sw $t1 1112($t0)
    sw $t1 1120($t0)
    sw $t1 1132($t0)
    sw $t1 1140($t0)
    sw $t1 1148($t0)
    li $v0 1
    j Fin_Spray
    #Cas Etat_1 spray en haut
    Etat_1_Spray:
    li $t1 1
    sw $t1 1060($t0)
    sw $t1 1068($t0)
    sw $t1 1088($t0)
    sw $t1 1096($t0)
    li $v0 2

  Fin_Spray:
  jr $ra

# Calcul du score
#entrée $a0 score à décrementer
#$a1 vaut 0 si defaite 1 sinon
#retour $v0 score du joueur
Calcul_score:
#Prologue
  addiu $sp $sp -8
  sw $ra 0($sp)
  sw $s3 4($sp)

#Corps
  lw $s3 PointsBase # Points au début de la partie
  sub $s3 $s3 $a0
  bgtz $a1 Calcul_score_victoire

  #defaite
  jal verif_gagne		 #renvoie le nombre d'alien Vivant dans $v0
  lw $t0 CoutPointsAliensVivants # Points perdus par aliens restants
  mul $v0 $v0 $t0
  sub $s3 $s3 $v0
  j fin_Calcul_score

  #victoire
 Calcul_score_victoire:
  jal verif_defaite_extermination	#Renvoie le nombre de pixel de batiments restant dans $v0
  lw $t0 PointsPixelsBatiments # Points gagnés par pixel de batiment restant
  mul $v0 $v0 $t0
  add $s3 $s3 $v0
  lw $t0 PointsVieRestantes # Points par vie restantes
  lw $t2 players_life
  mul $t2 $t2 $t0
  add $s3 $s3 $t2


 fin_Calcul_score:
  move $v0 $s3

#Epilogue
  lw $ra 0($sp)
  lw $s3 4($sp)
  addi $sp $sp 8
  jr $ra


#Permet de skip un jal en cas de press 5
#Prend en antrée dans $a0 a valeur du jal ou aller en cas de skip

break:
#Prologue
  addiu $sp $sp -8
  sw $s0 0($sp)
  sw $ra 4($sp)

#Corps
  move $s0 $a0
  jal keyStroke
  move $ra $s0
  bnez $v0 fin_break
  lw $ra 4($sp)

#Epilogue
  fin_break:
    lw $s0 0($sp)
    addi $sp $sp 8
    jr $ra


#Musique de l'écran titre
musique_ecran_titre:
 #prologue
 addiu $sp $sp -4
 sw $ra 0($sp)

 #Corps
 li $a2 17
 li $a3 60

 #Mesure 1
 li $a0 45	# La
 li $a1 700
 li $v0 31
 syscall
 li $a0 650
 li $v0 32
 syscall

 li $a0 57	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 900
 li $v0 31
 syscall
 li $a0 900
 li $v0 32
 syscall

 li $a0 57	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 710
 li $v0 31
 syscall
 li $a0 750
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 48	# Do
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 50	# Re
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 52	# Mi
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 48	# Do
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 lw $a0 0($sp)
 jal break

 #Mesure 2
 li $a2 32
 li $a3 80
 li $a0 45	# La (basse)
 li $a1 1600
 li $v0 31
 syscall
 li $a2 17
 li $a3 60

 li $a0 45	# La
 li $a1 700
 li $v0 31
 syscall
 li $a0 650
 li $v0 32
 syscall

 li $a0 57	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 900
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a2 32
 li $a3 80
 li $a0 41	# Fa (basse)
 li $a1 1200
 li $v0 31
 syscall
 li $a2 17
 li $a3 60

 li $a0 500
 li $v0 32
 syscall

 li $a0 57	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 400
 li $v0 32
 syscall

 li $a2 32
 li $a3 80
 li $a0 40	# Mi (basse)
 li $a1 1600
 li $v0 31
 syscall
 li $a2 17
 li $a3 60

 li $a0 45	# La
 li $a1 710
 li $v0 31
 syscall

 li $a0 750
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 48	# Do
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 50	# Re
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a2 32
 li $a3 80
 li $a0 40	# Mi (basse)
 li $a1 1200
 li $v0 31
 syscall
 li $a2 17
 li $a3 60

 li $a0 45	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 52	# Mi
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a0 48	# Do
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

  lw $a0 0($sp)
  jal break

 #Mesure 3
 li $a0 45	# La (basse)

 li $a2 32
 li $a3 80
 li $a1 1600
 li $v0 31
 syscall
 li $a2 17
 li $a3 60

 li $a0 45	# La
 li $a1 800
 li $v0 31
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 150
 li $v0 32
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 150
 li $v0 32
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 350
 li $v0 32
 syscall


 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 33
 syscall
 li $a2 17
 li $a3 60

 li $a0 57	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 100
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 800
 li $v0 31
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 200
 li $v0 32
 syscall


 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 59	# Si (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 200
 li $v0 32
 syscall

 li $a2 32
 li $a3 80
 li $a0 41	# Fa (basse)
 li $a1 1200
 li $v0 31
 syscall
 li $a2 17
 li $a3 60

 li $a0 100
 li $v0 32
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 59	# Si (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 59	# Si (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 200
 li $v0 32
 syscall


 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 59	# Si (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 350
 li $v0 32
 syscall

 li $a0 57	# La
 li $a1 400
 li $v0 31
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 59	# Si (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a2 32
 li $a3 80
 li $a0 40	# Mi (basse)
 li $a1 1600
 li $v0 31
 syscall
 li $a2 17
 li $a3 60

 li $a0 45	# La
 li $a1 800
 li $v0 31
 syscall

  lw $a0 0($sp)
  jal break

 # Mesure 4

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 60	# Do (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 150
 li $v0 32
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 60	# Do (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 150
 li $v0 32
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 60	# Do (mélodie)
 li $a1 150
 li $v0 31
 syscall

 li $a2 17
 li $a3 60
 li $a0 45	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 350
 li $v0 32
 syscall

 li $a0 48	# Do
 li $a1 400
 li $v0 31
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 60	# Do (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 350
 li $v0 32
 syscall

 li $a0 45	# La
 li $a1 400
 li $v0 31
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 60	# Do (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 250
 li $v0 32
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 62	# Re (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 250
 li $v0 32
 syscall

 li $a0 50	# Re
 li $a1 400
 li $v0 31
 syscall

 li $a2 32
 li $a3 80
 li $a0 40	# Mi (basse)
 li $a1 1200
 li $v0 31
 syscall
 li $a2 17
 li $a3 60
 li $a0 100
 li $v0 32
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 62	# Re (mélodie)
 li $a1 150
 li $v0 31
 syscall

 li $a0 45	# La
 li $a1 400
 li $v0 31
 syscall
 li $a0 300
 li $v0 32
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 62	# Re (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 150
 li $v0 32
 syscall

 li $a0 52	# Mi
 li $a1 400
 li $v0 31
 syscall

 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 62	# Re (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 400
 li $v0 32
 syscall


 li $a0 48	# Do
 li $a1 400
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 57	# La (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a2 8
 li $a3 100
 li $a0 62	# Re (mélodie)
 li $a1 150
 li $v0 31
 syscall
 li $a0 250
 li $v0 32
 syscall

 #Epilogue
 lw $ra 0($sp)
 addiu $sp $sp 4
 jr $ra



#Musique de l'écran de défaite
#$a0 = pitch (0-127)
#$a1 = duration in milliseconds
#$a2 = instrument (0-127)
#$a3 = volume (0-127)
music_defaite:
 #prologue
 addiu $sp $sp -4
 sw $ra 0($sp)

 #Corps
 li $a2 4
 li $a3 60
 #Mesure 1
 li $a0 67	# Sol
 li $a1 1800
 li $v0 31
 syscall

 li $a0 500
 li $v0 32
 syscall

 li $a0 75	# Mib
 li $a1 1300
 li $v0 33
 syscall

 li $a0 68	# Lab
 li $a1 1800
 li $v0 31
 syscall

 li $a0 500
 li $v0 32
 syscall

 li $a0 77	# Fa
 li $a1 1300
 li $v0 33
 syscall

 lw $a0 0($sp)
 jal break

 #Mesure 2
 li $a0 65	# Fa
 li $a1 1800
 li $v0 31
 syscall

 li $a0 500
 li $v0 32
 syscall

 li $a0 74	# Re
 li $a1 1300
 li $v0 33
 syscall

 li $a0 75	# Mib
 li $a1 1800
 li $v0 31
 syscall

 li $a0 72	# Do
 li $a1 1700
 li $v0 31
 syscall

 li $a0 500
 li $v0 32
 syscall

 li $a0 72	# Do
 li $a1 1600
 li $v0 31
 syscall

 li $a0 67	# Sol
 li $a1 1600
 li $v0 33
 syscall

 lw $a0 0($sp)
 jal break

 # 3ème mesure
 li $a3 80

 li $a0 60	# Do
 li $a1 1800
 li $v0 31
 syscall

 li $a0 63	# Mib
 li $a1 1700
 li $v0 31
 syscall

 li $a0 67	# Sol
 li $a1 1700
 li $v0 31
 syscall

 li $a0 500
 li $v0 32
 syscall

 li $a0 63	# Mib
 li $a1 1300
 li $v0 31
 syscall

 li $a0 67	# Sol
 li $a1 1300
 li $v0 31
 syscall

 li $a0 75	# Re
 li $a1 1300
 li $v0 33
 syscall

 li $a0 72	# Do
 li $a1 300
 li $v0 31
 syscall

 li $a0 75	# Mib
 li $a1 300
 li $v0 31
 syscall

 li $a0 79	# Sol
 li $a1 400
 li $v0 33
 syscall


 li $a0 72	# Do
 li $a1 300
 li $v0 31
 syscall

 li $a0 75	# Mib
 li $a1 300
 li $v0 31
 syscall

 li $a0 80	# Lab
 li $a1 400
 li $v0 33
 syscall


 li $a0 72	# Do
 li $a1 1300
 li $v0 31
 syscall

 li $a0 75	# Mib
 li $a1 1300
 li $v0 31
 syscall

 li $a0 68	# Lab
 li $a1 1300
 li $v0 33
 syscall

 lw $a0 0($sp)
 jal break

 #Mesure 4
 li $a0 71	# Si
 li $a1 1700
 li $v0 31
 syscall

 li $a0 74	# Re
 li $a1 1700
 li $v0 31
 syscall

 li $a0 79	# Sol
 li $a1 1800
 li $v0 31
 syscall

 li $a0 500
 li $v0 32
 syscall

 li $a0 71	# Si
 li $a1 1300
 li $v0 31
 syscall

 li $a0 74	# Re
 li $a1 1300
 li $v0 31
 syscall

 li $a0 67	# Sol
 li $a1 1300
 li $v0 33
 syscall

 li $a0 500
 li $v0 32
 syscall

 li $a0 82	# La#
 li $a1 150
 li $v0 31
 syscall

 li $a0 100
 li $v0 32
 syscall

 li $a0 83	# Si
 li $a1 150
 li $v0 31
 syscall

 li $a0 100
 li $v0 32
 syscall

 li $a0 84	# Do
 li $a1 2000
 li $v0 33
 syscall


 #Epilogue
 lw $ra 0($sp)
 addiu $sp $sp 4
 jr $ra


 #Joue la musique de victoire
 musique_victoire:
  #prologue
  addiu $sp $sp -4
  sw $ra 0($sp)

  #Corps
  li $a2 0
  li $a3 100

  #Mesure 1
  li $a0 58	# Sib
  li $a1 100
  li $v0 31
  syscall
  li $a0 70	# Sib
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  li $a0 62	# Re
  li $a1 100
  li $v0 31
  syscall
  li $a0 74	# Re
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  li $a0 65	# Fa
  li $a1 300
  li $v0 31
  syscall
  li $a0 77	# Fa
  li $a1 300
  li $v0 31
  syscall
  li $a0 400
  li $v0 32
  syscall


  li $a0 59	# Si
  li $a1 100
  li $v0 31
  syscall
  li $a0 71	# Si
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  li $a0 62	# Re
  li $a1 100
  li $v0 31
  syscall
  li $a0 74	# Re
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  li $a0 65	# Fa
  li $a1 300
  li $v0 31
  syscall
  li $a0 77	# Fa
  li $a1 300
  li $v0 31
  syscall
  li $a0 400
  li $v0 32
  syscall


  li $a0 68	# Lab
  li $a1 100
  li $v0 31
  syscall
  li $a0 80	# Lab
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  li $a0 69	# La
  li $a1 300
  li $v0 31
  syscall
  li $a0 81	# La
  li $a1 300
  li $v0 31
  syscall
  li $a0 350
  li $v0 32
  syscall

  li $a0 62	# Re
  li $a1 600
  li $v0 31
  syscall
  li $a0 74	# Re
  li $a1 600
  li $v0 31
  syscall
  li $a0 650
  li $v0 32
  syscall


  li $a0 62	# Re
  li $a1 100
  li $v0 31
  syscall
  li $a0 74	# Re
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  lw $a0 0($sp)
  jal break

  #Mesure 2
  li $a0 65	# Fa
  li $a1 100
  li $v0 31
  syscall
  li $a0 77	# Fa
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  li $a0 70	# Sib
  li $a1 100
  li $v0 31
  syscall
  li $a0 82	# Sib
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  li $a0 74	# Re
  li $a1 100
  li $v0 31
  syscall
  li $a0 86	# Re
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  li $a0 68	# Lab
  li $a1 100
  li $v0 31
  syscall
  li $a0 80	# Lab
  li $a1 100
  li $v0 31
  syscall
  li $a0 200
  li $v0 32
  syscall

  li $a0 69	# La
  li $a1 550
  li $v0 31
  syscall
  li $a0 81	# La
  li $a1 550
  li $v0 31
  syscall
  li $a0 600
  li $v0 32
  syscall

  li $a0 65	# Fa
  li $a1 400
  li $v0 31
  syscall
  li $a0 77	# Fa
  li $a1 400
  li $v0 31
  syscall
  li $a0 1000
  li $v0 32
  syscall

  li $a0 77	# Fa
  li $a1 100
  li $v0 31
  syscall
  li $a0 89	# Fa
  li $a1 100
  li $v0 31
  syscall
  li $a0 350
  li $v0 32
  syscall

  #Epilogue
  lw $ra 0($sp)
  addiu $sp $sp 4
  jr $ra

  #Asciiz_To_Nous
  #Prend une valeur asciiz dans $a0 et renvoie sa Position dans notre Alphabet de 0 à 40
  Ascii_To_Nous:
  #corps
    subi $v0 $a0 65
    bgt $v0 -1 Fin_Ascii_To_Nous		#test si c'est un nombre ou une lettre
    addi $v0 $v0 43			#Si c'est un nombre changer le placement
    Fin_Ascii_To_Nous:
    jr $ra

  #Affiche_Pseudo
  #entrée :	$a0 le frame Buffer déjà placé
  #		$a1 Tableau de code ascii
  #		$a2 le nombre de caractères à afficher
  #		$a3 l'adresse de la couleur à afficher
  Affiche_Pseudo:
  #Prologue
    addiu $sp $sp -24
    sw $ra 0($sp)
    sw $s0 4($sp)
    sw $s1 8($sp)
    sw $s2 12($sp)
    sw $s3 16($sp)
    lw $s4 20($sp)

  #Corps


    move $s0 $a0
    move $s1 $a1
    la $s2 Alphabet
    move $s3 $a3
    move $s4 $a2

    boucle_Affiche_Pseudo:
       lbu $a0 0($s1)
       jal Ascii_To_Nous
       li $t0 4
       mul $v0 $v0 $t0
       add $a0 $s2 $v0
       lw $a0 0($a0)
       move $a2 $s0			#initialise les autres variables et print la lettre
       move $a1 $s3
       jal affiche_tableau

       subi $s4 $s4 1
       addi $s1 $s1 1
       addi $s0 $s0 24
       bnez $s4 boucle_Affiche_Pseudo

  #epilogue
     lw $ra 0($sp)
     lw $s0 4($sp)
     lw $s1 8($sp)
     lw $s2 12($sp)
     lw $s3 16($sp)
     lw $s4 20($sp)
     addi $sp $sp 24
     jr $ra

  #Affiche_L'écran des High_Score
  #Affiche les noms suivis des scores des 5 premier joueurs
  #Prend en entrée $a0 l'adresse du tableau de taille 7*5 et dans $a1 L'emplacement du score du joueur entre 0 et 5
  Affiche_Ecran_High_Score:

  #Prologue
    addiu $sp $sp -24
    sw $ra 0($sp)
    sw $s0 4($sp)
    sw $s1 8($sp)
    sw $s2 12($sp)
    sw $s3 16($sp)
    sw $s4 20($sp)

  #Corps
    la $s0 frameBuffer
    addi $s0 $s0 4776
    move $s1 $a0
    la $s2 shooterColor
    la $s3 Alphabet
    move $s4 $a1

    	lw $a0 28($s3)		#H
    	move $a1 $s2
    	move $a2 $s0
    	jal affiche_tableau

    	addi $s0 $s0 16
    	lw $a0 32($s3)		#I
    	move $a1 $s2
    	move $a2 $s0
    	jal affiche_tableau

    	addi $s0 $s0 20
    	lw $a0 24($s3)		#G
    	move $a1 $s2
    	move $a2 $s0
    	jal affiche_tableau

    	addi $s0 $s0 20
    	lw $a0 28($s3)		#H
    	move $a1 $s2
    	move $a2 $s0
    	jal affiche_tableau

    	addi $s0 $s0 32
    	lw $a0 72($s3)		#S
    	move $a1 $s2
    	move $a2 $s0
    	jal affiche_tableau

    	addi $s0 $s0 20
    	lw $a0 8($s3)		#C
    	move $a1 $s2
    	move $a2 $s0
    	jal affiche_tableau

    	addi $s0 $s0 20
    	lw $a0 56($s3)		#O
    	move $a1 $s2
    	move $a2 $s0
    	jal affiche_tableau

    	addi $s0 $s0 20
    	lw $a0 68($s3)		#R
    	move $a1 $s2
    	move $a2 $s0
    	jal affiche_tableau

    	addi $s0 $s0 20
    	lw $a0 16($s3)		#E
    	move $a1 $s2
    	move $a2 $s0
    	jal affiche_tableau


    	addi $s0 $s0 4936
    	li $s3 5
    	boucle_affiche_ligne_score:
    	  la $s2 enemyColor3


    	  move $a0 $s0			#Affiche le pseudo
    	  move $a1 $s1
    	  li $a2 3
    	  move $a3 $s2
    	  jal Affiche_Pseudo

    	  addi $s0 $s0 128
    	  addi $s1 $s1 3
    	  la $s2 Couleur_Score

    	  move $a0 $s0			#Affiche le score
    	  move $a3 $s2
    	  move $a1 $s1
    	  li $a2 4
    	  jal Affiche_Pseudo

    	  addi $s0 $s0 3456		#incremente les compteurs et retourne à la ligne
    	  addi $s1 $s1 4
    	  subi $s3 $s3 1
    	  bgtz $s3 boucle_affiche_ligne_score


    	  la $s0 frameBuffer		#affiche le cadre
    	  addi $a0 $s0 2156
    	  li $a1 73
    	  li $a2 53
    	  la $a3 buildingColor
    	  lw $a3 0($a3)
    	  jal Affiche_Cadre_Quadrille

    	  #Affiche Le premier Humain sur l'ecrans de Fin
    	  la $s3 Alphabet
    	  addi $s3 $s3 192
    	  la $s2 Couleur_Blanc

    	  addi $s0 $s0 5656		#Partie haut gauche
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 0($s3)
    	  jal affiche_tableau

    	  addi $s0 $s0 20		#Partie Haut droite
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 4($s3)
    	  jal affiche_tableau

   	  addi $s0 $s0 2540		#Partie Bas Gauche
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 8($s3)
   	  jal affiche_tableau

   	  addi $s0 $s0 20		#Partie Bas droite
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 12($s3)
   	  jal affiche_tableau

   	  #Affiche Le second Humain sur l'ecrans de Fin

   	  addi $s0 $s0 5648		#Partie haut gauche
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 0($s3)
    	  jal affiche_tableau

    	  addi $s0 $s0 20		#Partie Haut droite
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 4($s3)
    	  jal affiche_tableau

   	  addi $s0 $s0 2540		#Partie Bas Gauche
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 8($s3)
   	  jal affiche_tableau

   	  addi $s0 $s0 20		#Partie Bas droite
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 12($s3)
   	  jal affiche_tableau

    	  #Affiche Le troisième Humain sur l'ecrans de Fin

   	  addi $s0 $s0 5588		#Partie haut gauche
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 0($s3)
    	  jal affiche_tableau

    	  addi $s0 $s0 20		#Partie Haut droite
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 4($s3)
    	  jal affiche_tableau

   	  addi $s0 $s0 2540		#Partie Bas Gauche
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 8($s3)
   	  jal affiche_tableau

   	  addi $s0 $s0 20		#Partie Bas droite
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 12($s3)
   	  jal affiche_tableau

	  #Affiche Le premier Alien sur l'ecrans de Fin

    	  subi $s3 $s3 16
    	  subi $s0 $s0 24196
    	  la $s2 enemyColor3

    	  addi $s0 $s0 5656		#Partie haut gauche
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 0($s3)
    	  jal affiche_tableau

    	  addi $s0 $s0 20		#Partie Haut droite
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 4($s3)
    	  jal affiche_tableau

   	  addi $s0 $s0 2540		#Partie Bas Gauche
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 8($s3)
   	  jal affiche_tableau

   	  addi $s0 $s0 20		#Partie Bas droite
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 12($s3)
   	  jal affiche_tableau

   	  #Affiche Le second Alien sur l'ecrans de Fin

   	  subi $s3 $s3 16
   	  la $s2 enemyColor2

   	  addi $s0 $s0 5576		#Partie haut gauche
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 0($s3)
    	  jal affiche_tableau

    	  addi $s0 $s0 20		#Partie Haut droite
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 4($s3)
    	  jal affiche_tableau

   	  addi $s0 $s0 2540		#Partie Bas Gauche
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 8($s3)
   	  jal affiche_tableau

   	  addi $s0 $s0 20		#Partie Bas droite
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 12($s3)
   	  jal affiche_tableau

    	  #Affiche Le troisième Alien sur l'ecrans de Fin

   	  addi $s3 $s3 16
   	  la $s2 enemyColor1

   	  addi $s0 $s0 5640		#Partie haut gauche
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 0($s3)
    	  jal affiche_tableau

    	  addi $s0 $s0 20		#Partie Haut droite
    	  move $a1 $s2
    	  move $a2 $s0
    	  lw $a0 4($s3)
    	  jal affiche_tableau

   	  addi $s0 $s0 2540		#Partie Bas Gauche
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 8($s3)
   	  jal affiche_tableau

   	  addi $s0 $s0 20		#Partie Bas droite
	  move $a1 $s2
   	  move $a2 $s0
   	  lw $a0 12($s3)
   	  jal affiche_tableau


   	  move $a0 $s4
   	  jal Curseur_High_Score_Player

  #Epilogue
    lw $ra 0($sp)
    lw $s0 4($sp)
    lw $s1 8($sp)
    lw $s2 12($sp)
    lw $s3 16($sp)
    lw $s4 20($sp)
    addi $sp $sp 24
    jr $ra

#Curseur_High_Score_Player
#Place un Curseur devant le score du joueur si il fait parti du tableau des high score
#entrée: 	$a0 prend la position du joueur dans le tableau 0/4 ou 5 si hors tableau
Curseur_High_Score_Player:

#Corps
  la $t0 frameBuffer
  lw $t1 buildingColor

  addi $t0 $t0 9856
  beqz $a0 Print_Curseur_High_Score_Player			#Déplace l'emplacement ou afficher le curseur en fonction de $a0
  addi $t0 $t0 3584
  beq $a0 1 Print_Curseur_High_Score_Player
  addi $t0 $t0 3584
  beq $a0 2 Print_Curseur_High_Score_Player
  addi $t0 $t0 3584
  beq $a0 3 Print_Curseur_High_Score_Player
  addi $t0 $t0 3584
  beq $a0 4 Print_Curseur_High_Score_Player
  addi $t0 $t0 3584
  j fin_Curseur_High_Score_Player

  Print_Curseur_High_Score_Player:
    sw $t1 0($t0)
    sw $t1 512($t0)
    sw $t1 516($t0)
    sw $t1 1024($t0)
    sw $t1 1028($t0)
    sw $t1 1032($t0)
    sw $t1 1536($t0)
    sw $t1 1540($t0)
    sw $t1 2048($t0)

  fin_Curseur_High_Score_Player:
  jr $ra


#Reset Toutes les valeurs modifiées pour pouvoir remcommer dans les conditions initiales
reset_variables:
#corps

  #Reset has_lost

  li $t0 0
  sw $t0 has_lost

  #reset de la box des aliens
  li $t0 71
  sw $t0 boxSizeX

  li $t0 27
  sw $t0 boxSizeY

  li $t0 0
  sw $t0 boxTopPosX

  sw $t0 boxTopPosY

  sw $t0 shotsInFlight

  li $t0 3
  sw $t0 enemiesRows

  li $t0 5
  sw $t0 enemiesPerRow

  li $t0 2
  sw $t0 boxDirX

  #reset du curseur du joueur
  li $t0 58
  sw $t0 shooterPosX

  la $t0 shooter
  li $t1 1
  sw $t1 160($t0)
  sw $t1 200($t0)
  sw $t1 216($t0)
  sw $t1 248($t0)
  sw $t1 116($t0)
  sw $t1 140($t0)
  sw $t1 172($t0)
  sw $t1 188($t0)

  #reset de la vie des ennemies
  la $t0 enemiesLife
  li $t1 1
  li $t2 15
  boucle_reset_enemiesLife:
    sw $t1 0($t0)
    addi $t0 $t0 4
    subi $t2 $t2 1
    bnez $t2 boucle_reset_enemiesLife

   #reset les buildings
   la $t0 building
   la $t1 building_save
   li $t2 192
   boucle_reset_buildings:
     lw $t3 0($t1)
     sw $t3 0($t0)
     addi $t1 $t1 4
     addi $t0 $t0 4
     subi $t2 $t2 1
     bnez $t2 boucle_reset_buildings

#epilogue
  jr $ra


#Affiche le message de victoire sur l'écran
#Argument : $a0 = position haut gauche du message dans le frame buffer
affiche_victoire:
 #prologue
  addi $sp $sp -16
  sw $s2 12($sp)
  sw $s1 8($sp)
  sw $s0 4($sp)
  sw $ra 0($sp)

 #corps
    move $s0 $a0
    la $s1 Couleur_Message_Victoire
    la $s2 Alphabet



    lw $a0 84($s2)		#V
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 8($s2)		#C
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 76($s2)		#T
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 56($s2)		#O
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 144($s2)		#!
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 12
    lw $a0 144($s2)		#!
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau


 #epilogue
  lw $s2 12($sp)
  lw $s1 8($sp)
  lw $s0 4($sp)
  lw $ra 0($sp)
  addi $sp $sp 16
  jr $ra


#Affiche le message de défaite sur l'écran
#Argument : $a0 = position haut gauche du message dans le frame buffer
affiche_defaite:
 #prologue
  addi $sp $sp -16
  sw $s2 12($sp)
  sw $s1 8($sp)
  sw $s0 4($sp)
  sw $ra 0($sp)

 #corps
  move $s0 $a0
  la $s1 Couleur_Message_Victoire
  la $s2 Alphabet

    lw $a0 24($s2)		#G
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 0($s2)		#A
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 48($s2)		#M
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau


    addi $s0 $s0 36
    lw $a0 56($s2)		#O
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 84($s2)		#V
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 144($s2)		#!
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 12
    lw $a0 144($s2)		#!
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

 #epilogue
  lw $s2 12($sp)
  lw $s1 8($sp)
  lw $s0 4($sp)
  lw $ra 0($sp)
  addi $sp $sp 16
  jr $ra


#Affiche le score à l'écran
#Argument : $a0 = position haut gauche du message dans le frame buffer
		#$a1 = score
affiche_score:
 #prologue
  addi $sp $sp -24
  sw $s4 20($sp)
  sw $s3 16($sp)
  sw $s2 12($sp)
  sw $s1 8($sp)
  sw $s0 4($sp)
  sw $ra 0($sp)

 #corps
  move $s0 $a0
  la $s1 Couleur_Message_Score
  la $s2 Alphabet
  move $s3 $a1

    lw $a0 72($s2)		#S
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 8($s2)		#C
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 56($s2)		#O
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 152($s2)		#!
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau


  la $s2 104($s2)
  li $s4 1000
  la $s1 Couleur_Score
  boucle_affiche_score:
   div $t1 $s3 $s4		#Récupère le premier chiffre
   li $t2 4			#Décale l'alphabet pour pointer sur le bon chiffre
   mul $t1 $t1 $t2
   move $t3 $s2
   add $t3 $t3 $t1
   add $s0 $s0 20		#Initialise l'impression du chiffre
   move $a2 $s0
   move $a1 $s1
   lw $a0 0($t3)
   jal affiche_tableau

   move $a0 $s3			#Retire le premier chiffre au score
   move $a1 $s4
   jal modulo
   move $s3 $v0
   li $t1 10			#Divise l'élément permettant de sortir le premier chiffre
   div $s4 $s4 $t1
   bgtz $s4 boucle_affiche_score

 #epilogue
  lw $s4 20($sp)
  lw $s3 16($sp)
  lw $s2 12($sp)
  lw $s1 8($sp)
  lw $s0 4($sp)
  lw $ra 0($sp)
  addi $sp $sp 24
  jr $ra


#Affiche_Ecran_Choix_Pseudo
#Affiche le texte de l'ecran de choix de pseudo
Affiche_Ecran_Choix_Pseudo:

#prologue
  addi $sp $sp -16
  sw $s2 12($sp)
  sw $s1 8($sp)
  sw $s0 4($sp)
  sw $ra 0($sp)

 #corps
  la $s0 frameBuffer
  addi $s0 $s0 2632
  la $s1 Couleur_Score
  la $s2 Alphabet

    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 52($s2)		#N
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 76($s2)		#T
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 100($s2)		#Z
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 40
    lw $a0 84($s2)		#V
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 56($s2)		#O
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 76($s2)		#T
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 24
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 40
    lw $a0 52($s2)		#N
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 56($s2)		#O
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 48($s2)		#M
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 28
    lw $a0 152($s2)		#:
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20268
    lw $a0 84($s2)		#V
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 0($s2)		#A
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 44($s2)		#L
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 16
    lw $a0 32($s2)		#I
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 12($s2)		#D
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 16($s2)		#E
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau

    addi $s0 $s0 20
    lw $a0 68($s2)		#R
    move $a1 $s1
    move $a2 $s0
    jal affiche_tableau


#Epilogue
  lw $s2 12($sp)
  lw $s1 8($sp)
  lw $s0 4($sp)
  lw $ra 0($sp)
  addi $sp $sp 16
  jr $ra

#Menu_Choix_Pseudo
#Effet de bord: Remplacement de text_pseudo par le pseudo entré par le joueur via les touches 4/5/6
Menu_Choix_Pseudo:
#Prologue
  addiu $sp $sp -12
  sw $ra 0($sp)
  sw $s0 4($sp)
  sw $s1 8($sp)

#Corps
  jal keyStroke
  jal Affiche_Ecran_Choix_Pseudo
  lw $s0 text_pseudo
  li $s1 0
  move $a0 $s0
  jal MAJ_Affichage_Pseudo
  move $a0 $s1
  jal MAJ_Affichage_Curseur_Pseudo
  boucle_Menu_Choix_Pseudo:
     li $a0 25
     jal sleep
     jal keyStroke
     beq $v0 1 Menu_Choix_Pseudo_Press_4		#Jump selon la touche préssé ou relance la boucle
     beq $v0 2 Menu_Choix_Pseudo_Press_6
     beq $v0 3 Menu_Choix_Pseudo_Press_5
     j boucle_Menu_Choix_Pseudo

  #Cas press 4
  Menu_Choix_Pseudo_Press_4:
    subi $s1 $s1 1			#Modifie le compteur et lui rentre sa valeur modulo 4 pour boucler le Menu
    move $a0 $s1
    li $a1 4
    jal modulo
    move $s1 $v0
    move $a0 $s1			#Change de place le Curseur
    jal MAJ_Affichage_Curseur_Pseudo

    #Son
      li $a0 80
      li $a1 150
      li $a2 112
      li $a3 80
      li $v0 33
      syscall

    j boucle_Menu_Choix_Pseudo

  #Cas press 6
  Menu_Choix_Pseudo_Press_6:
    addi $s1 $s1 1			#Modifie le compteur et lui rentre sa valeur modulo 4 pour boucler le Menu
    move $a0 $s1
    li $a1 4
    jal modulo
    move $s1 $v0
    move $a0 $s1
    jal MAJ_Affichage_Curseur_Pseudo

    #Son
      li $a0 80
      li $a1 150
      li $a2 112
      li $a3 80
      li $v0 33
      syscall

    j boucle_Menu_Choix_Pseudo

   #Cas press 5
   Menu_Choix_Pseudo_Press_5:
     beq $s1 3 Fin_Menu_Choix_Pseudo
     move $a0 $s0			#Modifie la valeur du texte du pseudo
     move $a1 $s1
     jal modifie_Pseudo
     move $s0 $v0
     move $a0 $s0
     jal MAJ_Affichage_Pseudo
     j boucle_Menu_Choix_Pseudo

   Fin_Menu_Choix_Pseudo:
     sw $s0 text_pseudo

     #Son validation
    li $a0 80
    li $a1 150
    li $a2 112
    li $a3 80
    li $v0 33
    syscall
    addi $a0 $a0 7
    addi $a1 $a1 50
    syscall

 #Epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  addi $sp $sp 12
  jr $ra


#Suprimme l'ancien affichage et print l'affichage actuel du Curseur
#entrée: 	$a0 Position du Curseur à afficher 0/1/2/3
MAJ_Affichage_Curseur_Pseudo:
#Prologue
  addiu $sp $sp -8
  sw $ra 0($sp)
  sw $s0 4($sp)
#Corps
  move $s0 $a0

  li $a0 20				#Clean L'anciens curseurs du haut
  li $a1 21
  li $a2 88
  li $a3 2
  jal cleanPartOfScreen

  li $a0 20				#Clean Les anciens Curseur du bas
  li $a1 34
  li $a2 88
  li $a3 2
  jal cleanPartOfScreen

  li $a0 39				#Clean L'ancien Curseur devant Valider
  li $a1 45
  li $a2 3
  li $a3 5
  jal cleanPartOfScreen

  la $t0 frameBuffer					#Déplace le frameBuffer en fonction de l'emplacement voulu
  addi $t0 $t0 10920
  beqz $s0 MAJ_Affichage_Curseur
  addi $t0 $t0 88
  beq $s0 1 MAJ_Affichage_Curseur
  addi $t0 $t0 88
  beq $s0  2 MAJ_Affichage_Curseur

  #Sinon curseur devant Valider
    addi $t0 $t0 12100
    lw $t1 enemyColor3
    sw $t1 0($t0)
    sw $t1 512($t0)
    sw $t1 516($t0)
    sw $t1 1024($t0)
    sw $t1 1028($t0)					#Print le curseur 2     sw $t1 7168($t0)
    sw $t1 1032($t0)
    sw $t1 1536($t0)
    sw $t1 1540($t0)
    sw $t1 2048($t0)
    j Fin_MAJ_Affichage_Curseur

  MAJ_Affichage_Curseur:				#Print le curseur 1
     lw $t1 enemyColor3
     sw $t1 0($t0)
     sw $t1 4($t0)
     sw $t1 8($t0)
     sw $t1 516($t0)
     sw $t1 6660($t0)					#Print le curseur 2
     sw $t1 7168($t0)
     sw $t1 7172($t0)
     sw $t1 7176($t0)

Fin_MAJ_Affichage_Curseur:
#Epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  addi $sp $sp 8
  jr $ra



#Suprimme l'ancien affichage et print l'affichage actuel du pseudo
#entrée:	$a0 nombre de Char à afficher
MAJ_Affichage_Pseudo:

#Prologue
  addiu $sp $sp -20
  sw $ra 0($sp)
  sw $s0 4($sp)
  sw $s1 8($sp)
  sw $s2 12($sp)
  sw $s3 16($sp)


#Corps
  la $s0 frameBuffer
  addi $s0 $s0 13656
  move $s1 $a0
  la $s2 Alphabet
  la $s3 shooterColor

  li $a0 20				#Clean L'ancien Pseudo
  li $a1 26
  li $a2 88
  li $a3 5
  jal cleanPartOfScreen

  boucle_Affiche_Pseudo_Menu:
       move $a0 $s1			#Place l'alphabet sur la bonne lettre
       li $a1 1000
       jal modulo
       move $a0 $v0
       jal Ascii_To_Nous
       li $t0 4
       mul $v0 $v0 $t0
       add $a0 $s2 $v0
       lw $a0 0($a0)
       move $a2 $s0			#initialise les autres variables et print la lettre
       move $a1 $s3
       jal affiche_tableau

       li $t0 1000			#Suprimme le troisième code, delace l'endroit à print et recommence
       div $s1 $s1 $t0
       subi $s0 $s0 88

       bnez $s1 boucle_Affiche_Pseudo_Menu

#Epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  lw $s2 12($sp)
  lw $s3 16($sp)
  addi $sp $sp 20
  jr $ra

#modifie_Pseudo
#entrée:	$a0 prend la valeur à modifier
#		$a1 prend la lettre à modifier 0/1/2
#sortie:	$v0 renvoie la nouvelle valeur du Pseudo
modifie_Pseudo:
#Prologue
  addiu $sp $sp -8
  sw $ra 0($sp)
  sw $s0 4($sp)

#Corps
  move $s0 $a0
  beqz $a1 modifie_Pseudo_Lettre_1
  beq $a1 1 modifie_Pseudo_Lettre_2

  #sinon modifie Lettre 3
  addi $s0 $s0 1			#incrémente la troisième lettre et la repase à A si elle dépase Z
  move $a0 $s0
  li $a1 1000
  jal modulo
  blt $v0 91 fin_modifie_Pseudo
  subi $s0 $s0 26
  j fin_modifie_Pseudo

  modifie_Pseudo_Lettre_2:
  addi $s0 $s0 1000			#incrémente la deuxième lettre et la repase à A si elle dépase Z
  move $a0 $s0
  li $a1 1000000
  jal modulo
  blt $v0 91000 fin_modifie_Pseudo
  subi $s0 $s0 26000
  j fin_modifie_Pseudo

  modifie_Pseudo_Lettre_1:
  addi $s0 $s0 1000000			#incrémente la première lettre et la repase à A si elle dépase Z
  blt $s0 91000000 fin_modifie_Pseudo
  subi $s0 $s0 26000000

  fin_modifie_Pseudo:
  move $v0 $s0

#Epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  addi $sp $sp 8
  jr $ra


# Ouvre un fichier
# Parametres :
#       $a0: pointeur vers le fichier à ouvrir
#		$a1: mode de lecture (0: read-only, 1:write-only, 9:write-only avec création si fichier inexistant + ajout à la fin)
# Retour : v0 pointeur vers le fichier
ouvrir_fichier:
  #prologue
  addi $sp $sp -4
  sw $ra 0($sp)

  # Corps
  li $v0, 13
  li $a2, 0
  syscall

  # epilogue
  lw $ra 0($sp)
  addi $sp $sp 4
  jr $ra

# Ferme un fichier
# Parametres : $a0 pointeur vers le fichier à fermer
fermer_fichier:
  #prologue
  addi $sp $sp -4
  sw $ra 0($sp)

  # Corps
  li $v0, 16
  syscall

  # epilogue
  lw $ra 0($sp)
  addi $sp $sp 4
  jr $ra


# Lis depuis un fichier
# parametres : $a0 pointeur vers le fichier
#		$a1 pointeur vers le stockage
#		$a2 taille à lire dans le fichier
lire_fichier:
  # prologue
  addi $sp $sp -4
  sw $ra 0($sp)

  # Lecture depuis le fichier
  li $v0, 14
  syscall

  #epilogue
  lw $ra 0($sp)
  addi $sp $sp 4
  jr $ra


# Ecris dans un fichier (le crée s'il n'existe pas)
# parametres : $a0 pointeur vers le fichier
# 		$a1 pointeur vers le texte à ecrire
#		$a2 taille de la chaine à ecrire
#Effet de bord : Crée un fichier dans le répertoire où se trouve le simulateur Mars
ecrire_fichier:
  # prologue
  addi $sp $sp -4
  sw $ra 0($sp)

  # Corps
  li $v0, 15
  syscall

  #epilogue
  lw $ra 0($sp)
  addi $sp $sp 4
  jr $ra

# Sauvegarde le tableau des highscores dans un fichier
# parametres :
#		$a0: adresse du fichier
#		$a1: adresse du tableau Pseudo-Score
#       $a2: taille du tableau
sauvegarde_highscores:
  # prologue
  addi $sp $sp -20
  sw $s3 16($sp)
  sw $s2 12($sp)
  sw $s1 8($sp)
  sw $s0 4($sp)
  sw $ra 0($sp)

  #corps
  move $s0 $a0
  move $s1 $a1
  move $s2 $a2

  # Sauvegarde tout le Tableau
  boucle_sauvegarde_highscore:
  subi $s2 $s2 1
      # Stocke les 7 caractères (3 Pseudo + 4 Scores)
      li $s3 7
      boucle_sauvegarde_un_highscore:
          # Ecris 1 caractère
          move $a0, $s0
          move $a1 $s1
          li $a2 1
          jal ecrire_fichier
          addi $s1 $s1 1 # Se déplace de 1 cran dans le tableau
          subi $s3 $s3 1
          bgtz $s3 boucle_sauvegarde_un_highscore

      #Rajoute un caractère de fin de ligne
      move $a0, $s0
      la $a1 saut_ligne
      li $a2 1
      jal ecrire_fichier

    bgtz $s2 boucle_sauvegarde_highscore

  #epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  lw $s2 12($sp)
  lw $s3 16($sp)
  addi $sp $sp 20
  jr $ra

# Charge un tableau avec le contenu du fichier
# Arguments :
#       $a0: pointeur vers le fichier
#		$a1: adresse du tableau de sauvegarde
#       $a2: taille du tableau de sauvegarde
charge_highscores:
  #prologue
  addi $sp $sp -24
  sw $s4 20($sp)
  sw $s3 16($sp)
  sw $s2 12($sp)
  sw $s1 8($sp)
  sw $s0 4($sp)
  sw $ra 0($sp)

  # Corps
  move $s0 $a0
  move $s1 $a1
  move $s2 $a2

  # Stocke les n highscores
  boucle_charge_highscores:
      # Stocke 7 caractères correspondant à un highscore (3char Pseudo + 4char Score)
      li $s3 7
      boucle_charge_un_highscores:
        # Stocke 1 caractère
        move $a0 $s0
        la $a1 0($s1)
        li $a2 1
        jal lire_fichier
        addi $s1 $s1 1 # Avance d'un cran dans le tableau
        subi $s3 $s3 1
        bgtz $s3 boucle_charge_un_highscores

        # Sauter le caractère de fin de ligne
        move $a0 $s0
        la $a1 devnull
        li $a2 1
        jal lire_fichier

      subi $s2 $s2 1
      bgtz $s2 boucle_charge_highscores

  # Epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  lw $s2 12($sp)
  lw $s3 16($sp)
  addi $sp $sp 20
  jr $ra


# Décale tout les highscores à partir d'un certain rang
# Arguments :
#		$a0 tableau des scores
#		$a1 taille du tableau (en nombre de joueurs, exemple 5)
#		$a2 indice du premier score à décaler (0, 1, 2, 3 ou 4)
decale_highscores:
  subi $t0 $a1 1
  beq $a2 $t0 fin_decale_highscores
  boucle_decale_highscores:
    # Se place sur l'indice à décaler
    subi $t0 $t0 1
    li $t2 7 # 7 cases par indice, 1 byte par case
    mul $t1 $t0 $t2
    add $t1 $a0 $t1
    # Décale les 7 cases de l'indice dans l'indice suivant
    move $t3 $zero
    boucle_decale_un_highscore:
      lbu $t4 0($t1)
      sb $t4 7($t1)
      addi $t1 $t1 1
      addi $t3 $t3 1
      blt $t3 $t2 boucle_decale_un_highscore

    bgt $t0 $a2 boucle_decale_highscores
  fin_decale_highscores:
  jr $ra


# Donne la position du joueur dans les highscores pour insertion
# arguments :
#		$a0 adresse du tableau des scores
#		$a1 taille du tableau (En nombre de highscore)
#		$a2 score du joueur
# Retour : v0 la position que le joueur devrait avoir dans le tableau (si $v0 = $a1, le joueur n'a pas atteint de highscore)
position_score:
  # Prologue
  addi $sp $sp -36
  sw $s7 32($sp)
  sw $s6 28($sp)
  sw $s5 24($sp)
  sw $s4 20($sp)
  sw $s3 16($sp)
  sw $s2 12($sp)
  sw $s1 8($sp)
  sw $s0 4($sp)
  sw $ra 0($sp)

  # Corps
  move $s0 $a0
  move $s1 $a1
  move $s2 $a2

  # On stocke chaque chiffre du score (pas de boucle car on stocke dans des variables différentes)
      # Stockage des milliers dans $s4 (encodage ascii)
      li $s3 1000
      div $t1 $s2 $s3
      addi $t1 $t1 48
      move $s4 $t1
      move $a0 $s2
      move $a1 $s3
      jal modulo
      move $s2 $v0

      # Stockage des centaines dans $s5 (encodage ascii)
      li $s3 100
      div $t1 $s2 $s3
      addi $t1 $t1 48
      move $s5 $t1
      move $a0 $s2
      move $a1 $s3
      jal modulo
      move $s2 $v0

      # Stockage des dizaines dans $s6 (encodage ascii)
      li $s3 10
      div $t1 $s2 $s3
      addi $t1 $t1 48
      move $s6 $t1
      move $a0 $s2
      move $a1 $s3
      jal modulo
      move $s7 $v0
      addi $s7 $s7 48 # Stockage des unités dans $s7 (encodage ascii)

  li $t0 0
  move $t1 $s0
  boucle_position_score:
    bge $t0 $s1 fin_boucle_position_score
    # Comparaison milliers
    lbu $t2 3($t1)
    bgt $s4 $t2 fin_boucle_position_score
    blt $s4 $t2 incrementation_boucle_position_score

    # Sinon comparaison centaines
    lbu $t2 4($t1)
    bgt $s5 $t2 fin_boucle_position_score
    blt $s5 $t2 incrementation_boucle_position_score

    # Sinon comparaison dizaines
    lbu $t2 5($t1)
    bgt $s6 $t2 fin_boucle_position_score
    blt $s6 $t2 incrementation_boucle_position_score

    # Sinon comparaison unite
    lbu $t2 6($t1)
    bgt $s7 $t2 fin_boucle_position_score

    incrementation_boucle_position_score:
      addi $t0 $t0 1
      addi $t1 $t1 7
      j boucle_position_score

  fin_boucle_position_score:
    move $v0 $t0

  # Epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  lw $s2 12($sp)
  lw $s3 16($sp)
  lw $s4 20($sp)
  lw $s5 24($sp)
  lw $s6 28($sp)
  lw $s7 32($sp)
  addi $sp $sp 36
  jr $ra


# Insere un joueur et son score dans le tableau
# Arguments :
#		$a0 adresse du tableau
#		$a1 position de l'insertion
#		$a2 adresse du nom du joueur (écris comme 3 caractères ascii concaténés)
#		$a3 Score du joueur
insere_highscore:
  # Prologue
  addi $sp $sp -32
  sw $s6 28($sp)
  sw $s5 24($sp)
  sw $s4 20($sp)
  sw $s3 16($sp)
  sw $s2 12($sp)
  sw $s1 8($sp)
  sw $s0 4($sp)
  sw $ra 0($sp)

  # Corps
  move $s0 $a0
  move $s1 $a1
  move $s2 $a2
  move $s3 $a3

  # Bien se positionner dans le tableau
  li $t0 7
  mul $t0 $t0 $s1
  add $s4 $t0 $s0

  # Stocker le pseudo
  lw $s5, 0($s2)
  li $s6 1000000
  # Le pseudo est constitué de 3 codes ascii concatenés
  # On cherche a stocker chacun de ses caractères séparemment
  boucle_transforme_caracteres_pseudo:
    div $t0 $s5 $s6
    sb $t0, 0($s4)
    addi $s4, $s4, 1
    move $a0 $s5
    move $a1 $s6
    jal modulo
    move $s5 $v0
    li $t1 1000
    div $s6 $s6 $t1
    bgtz $s6 boucle_transforme_caracteres_pseudo

  # Stocker le score chiffre par chiffre (format ascii)
  li $s5 1000
  boucle_stockage_score:
    div $t0 $s3 $s5
    addi $t0 $t0 48
    sb $t0 0($s4)
    addi $s4 $s4 1
    move $a0 $s3
    move $a1 $s5
    jal modulo
    move $s3 $v0
    li $t1 10
    div $s5 $s5 $t1
    bgtz $s5 boucle_stockage_score

  # Epilogue
  lw $ra 0($sp)
  lw $s0 4($sp)
  lw $s1 8($sp)
  lw $s2 12($sp)
  lw $s3 16($sp)
  lw $s4 20($sp)
  lw $s5 24($sp)
  lw $s6 28($sp)
  addi $sp $sp 32
  jr $ra


#Affiche l'ecran de victoire/défaite
#arguments :
	#$a0 : Score
	#$a1 : 0:Défaite || 1:Victoire
affiche_ecran_fin:
 #prologue
 addiu $sp $sp -20
 sw $ra 0($sp)
 sw $s0 4($sp)
 sw $s1 8($sp)
 sw $s2 12($sp)
 sw $s3 16($sp)

 #corps
 move $s0 $a0
 move $s1 $a1
 jal clean_screen
 la $s2 frameBuffer
 la $s3 Alphabet
 addi $s3 $s3 160

 beqz $s1 affiche_ecran_defaite

 #Cas Victoire

    #Affiche Le premier Humain sur l'ecrans de Fin

    addi $s3 $s3 32

    addi $s2 $s2 5692		#Partie haut gauche
    la $a1 buildingColor
    move $a2 $s2
    lw $a0 0($s3)
    jal affiche_tableau

    addi $s2 $s2 20		#Partie Haut droite
    la $a1 buildingColor
    move $a2 $s2
    lw $a0 4($s3)
    jal affiche_tableau

   addi $s2 $s2 2540		#Partie Bas Gauche
   la $a1 buildingColor
   move $a2 $s2
   lw $a0 8($s3)
   jal affiche_tableau

   addi $s2 $s2 20		#Partie Bas droite
   la $a1 buildingColor
   move $a2 $s2
   lw $a0 12($s3)
   jal affiche_tableau

 #Ecrit Le texte de Victoire
 add $s2 $s2 3672
 move $a0 $s2
 jal affiche_victoire
 li $t0 6136
 j affiche_ecran_score

#cas defaite
affiche_ecran_defaite:

  #Affiche Le premier Alien sur l'ecran de Fin

    addi $s2 $s2 5692		#Partie haut gauche
    la $a1 buildingColor
    move $a2 $s2
    lw $a0 0($s3)
    jal affiche_tableau

    addi $s2 $s2 20		#Partie Haut droite
    la $a1 buildingColor
    move $a2 $s2
    lw $a0 4($s3)
    jal affiche_tableau

   addi $s2 $s2 2540		#Partie Bas Gauche
   la $a1 buildingColor
   move $a2 $s2
   lw $a0 8($s3)
   jal affiche_tableau

   addi $s2 $s2 20		#Partie Bas droite
   la $a1 buildingColor
   move $a2 $s2
   lw $a0 12($s3)
   jal affiche_tableau

#Affiche le message de Défaite
  add $s2 $s2 3652
  move $a0 $s2
  jal affiche_defaite
  li $t0 6148
  addi $s3 $s3 16

#Affiche le score
 affiche_ecran_score:
  add $s2 $s2 $t0
  move $a0 $s2
  move $a1 $s0
  jal affiche_score

#Affiche cadre
  la $a0 frameBuffer
  addi $a0 $a0 1548
  li $a1 121
  li $a2 57
  lw $a3 buildingColor
  jal Affiche_Cadre_Quadrille

#Affiche le second Alien/Humain de l'écran de Fin

  addi $s2 $s2 5880		#Partie haut gauche
  la $a1 buildingColor
  move $a2 $s2
  lw $a0 0($s3)
  jal affiche_tableau

  addi $s2 $s2 20		#Partie Haut droite
  la $a1 buildingColor
  move $a2 $s2
  lw $a0 4($s3)
  jal affiche_tableau

  addi $s2 $s2 2540		#Partie Bas Gauche
  la $a1 buildingColor
  move $a2 $s2
  lw $a0 8($s3)
  jal affiche_tableau

  addi $s2 $s2 20		#Partie Bas droite
  la $a1 buildingColor
  move $a2 $s2
  lw $a0 12($s3)
  jal affiche_tableau


 #epilogue
 lw $ra 0($sp)
 lw $s0 4($sp)
 lw $s1 8($sp)
 lw $s2 12($sp)
 lw $s3 16($sp)
 addiu $sp $sp 20
 jr $ra


#Musique_fin de jeu joue la musique lié à la fin du jeu Victoire/défaite
#Entrée: $a0 vaut 1 si victoire 0 si défaite
musique_fin_jeu:
#prologue
  addiu $sp $sp -4
  sw $ra 0($sp)
#corps
  beqz $a0 musique_fin_jeu_defaite	#Joue la bonne musique de fin
    jal musique_victoire
    j musique_fin_jeu_fin
  musique_fin_jeu_defaite:
    jal music_defaite
#epilogue
  musique_fin_jeu_fin:
    lw $ra 0($sp)
    addi $sp $sp 4
    jr $ra


#Fin du jeu quand le joueur perd
a_perdu:
 la $a0 message_defaite
 jal print_string
 li $s0 0
 j Fin_De_Partie

#Fin du jeu quand le joueur Gagne
a_gagne:
 la $a0 message_victoire
 jal print_string
 li $s0 1

#Fin du jeu
Fin_De_Partie:
 move $a0 $s2
 move $a1 $s0
 jal Calcul_score
 move $s1 $v0
 move $a0 $s1
 jal print_int # Affichage console du score
 move $a0 $s1
 move $a1 $s0
 jal affiche_ecran_fin
 move $a0 $s0
 jal musique_fin_jeu
 jal clean_screen

 # allocation de 35 bytes (tableau[5:scores][7:3Pseudo+4Score])
 li $v0, 9
 li $a0, 35
 syscall
 move $s6, $v0 # $s6 contient l'adresse du tableau

 # Ouverture du fichier en lecture
 la $a0 filename
 li $a1 0
 jal ouvrir_fichier
 move $s5 $v0 # $s5 est un pointeur vers le fichier

 # Chargement des highscores depuis le fichier
 move $a0 $s5
 move $a1 $s6
 lw $a2 nombreDeScoresSauvegardes
 jal charge_highscores

 # Fermeture du fichier
 move $a0 $s5
 jal fermer_fichier

 # Classement du joueur
 move $a0 $s6
 li $a1 5
 move $a2 $s1
 jal position_score
 move $s3 $v0 # $s3 contient le classement du joueur
 lw $t0 nombreDeScoresSauvegardes


 bge $s3 $t0 sauter_inscription_classement
 # Si le joueur est dans le classement

 # Ecran de selection du pseudo
 jal Menu_Choix_Pseudo
 jal clean_screen

 # Décalage des classements inférieurs
 move $a0 $s6
 lw $a1 nombreDeScoresSauvegardes
 move $a2 $s3
 jal decale_highscores

 # Inscription au classement
 move $a0 $s6
 move $a1 $s3
 la $a2 text_pseudo
 move $a3 $s1
 jal insere_highscore

 # Création du ficier s'il n'existe pas
 la $a0 filename
 li $a1 9
 jal ouvrir_fichier
 move $s5 $v0

 # Ecriture dans le fichier pour qu'il ne soit pas vide
 move $a0 $s5
 la $a1 message_victoire
 li $a2 4
 jal ecrire_fichier

 # Fermeture du fichier
 move $a0 $s5
 jal fermer_fichier

 # Ouverture du fichier en ecriture
 la $a0 filename
 li $a1 1
 jal ouvrir_fichier
 move $s5 $v0

 # Mise à jour du fichier de sauvegarde des highscores (écrasement des anciens highscores)
 move $a0 $s5
 move $a1 $s6
 lw $a2 nombreDeScoresSauvegardes
 jal sauvegarde_highscores

 # Fermeture du fichier
 move $a0 $s5
 jal fermer_fichier

sauter_inscription_classement:

 move $a0 $s6
 move $a1 $s3
 jal Affiche_Ecran_High_Score		#Affiche les High Score suivis par le choix de rejouer
 li $a0 1500
 jal sleep
 jal clean_screen
 jal Menu_Fin
 beqz $v0 Restart
 jal clean_screen
 jal Affiche_Invaders_Start
 jal Message_Depart
 j quit

 #reset le jeu et redemarge devant le choix de difficulté
 Restart:
  jal reset_variables
  j Démarrage_du_jeu
